@extends('layouts.app')

@section('content')

    <div class="row row-sm">
      <div class="col-sm-6 col-xl-2 mg-t-20 mg-xl-t-0">
          <div class="bg-primary rounded overflow-hidden">
            <div class="pd-25 d-flex align-items-center">
              <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Today's Leads</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{ $total['pendingLeadsToday'] }}</p>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-xl-2 mg-t-20 mg-xl-t-0">
          <div class="bg-primary rounded overflow-hidden">
            <div class="pd-25 d-flex align-items-center">
              <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Today's Buyers</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{ $total['buyersLeadsToday'] }}</p>
              </div>
            </div>
          </div>
        </div>


  <div class="col-sm-6 col-xl-2 mg-t-20 mg-xl-t-0">
          <div class="bg-primary rounded overflow-hidden">
            <div class="pd-25 d-flex align-items-center">
              <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Total Buyers</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{ $total['buyersLeadsTotal'] }}</p>
              </div>
            </div>
          </div>
        </div>

          <div class="col-sm-6 col-xl-2 mg-t-20 mg-xl-t-0">
          <div class="bg-primary rounded overflow-hidden">
            <div class="pd-25 d-flex align-items-center">
              <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Pending Leads</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{ $total['pendingLeadsTotal'] }}</p>
              </div>
            </div>
          </div>
        </div>

         <div class="col-sm-6 col-xl-2 mg-t-20 mg-xl-t-0">
          <div class="bg-primary rounded overflow-hidden">
            <div class="pd-25 d-flex align-items-center">
              <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Total Leads</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{ $total['LeadsTotal'] }}</p>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
