    <!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo"><a href="{{ url('/') }}"><span></span>{{ config('app.name', 'Laravel') }}<span></span></a></div>
    <div class="br-sideleft sideleft-scrollbar">
      <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navigation</label>
      <ul class="br-sideleft-menu">
        <li class="br-menu-item">
          <a href="{{ route('dashboard') }}" class="br-menu-link {{ request()->is('dashboard') ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
            <span class="menu-item-label">Dashboard</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="{{ route('leads') }}" class="br-menu-link {{ request()->is('leads') ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-ios-paper-outline tx-22"></i>
            <span class="menu-item-label">Leads</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="{{ route('manual-leads') }}" class="br-menu-link {{ request()->is('manual-leads') ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-ios-paper-outline tx-22"></i>
            <span class="menu-item-label">Manual Leads</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="{{ route('offers-list') }}" class="br-menu-link {{ request()->is('offers/list') ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-bag tx-22"></i>
            <span class="menu-item-label">Offers</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="{{ route('network.index') }}" class="br-menu-link {{ request()->is('network') ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-earth tx-24"></i>
            <span class="menu-item-label">Network</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="{{ route('platform.index') }}" class="br-menu-link {{ request()->is('platform') ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-earth tx-24"></i>
            <span class="menu-item-label">Network Platform</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @role('admin')
        <li class="br-menu-item">
          <a href="{{ route('user.index') }}" class="br-menu-link {{ request()->is('user') ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-person tx-24"></i>
            <span class="menu-item-label">Manage Users</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endrole


      </ul><!-- br-sideleft-menu -->

      



      <br>
    </div><!-- br-sideleft -->