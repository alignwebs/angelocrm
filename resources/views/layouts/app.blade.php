<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    
    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.default.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/styles/metro/notify-metro.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
 
    <link href="{{ asset('theme/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/rickshaw.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/select2.min.css') }}" rel="stylesheet">

     <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('theme/css/bracket.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css">
    <style>
    table th { text-transform: capitalize; }
    table.dataTable tbody tr.selected:hover { color: #000 !important; }
    .table td, table td { vertical-align: middle; }

    label { font-weight: bold; }
    table label { margin-bottom: 0; }
    .modal-header {
    border-bottom: 1px solid #2d2e31;
    background: #2d2e31;
    color: #fff;
}
.modal-title {
    margin-bottom: 0;
    line-height: 1;
}
.close {
    float: right;
    font-size: 1.3125rem;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-shadow: 0 1px 0 #000;
    opacity: 0.9;
}
table.dataTable tbody td.select-checkbox:before, table.dataTable tbody td.select-checkbox:after, table.dataTable tbody th.select-checkbox:before, table.dataTable tbody th.select-checkbox:after {
    display: block;
    position: absolute;
    top: 12px;
    left: 50%;
    width: 16px;
    height: 16px;
    box-sizing: border-box;
}

table.dataTable tr.selected td.select-checkbox:after, table.dataTable tr.selected th.select-checkbox:after {
    content: '\2714';
    margin-top: 2px;
    margin-left: -6px;
    text-align: center;
    text-shadow: 1px 1px #B0BED9, -1px -1px #B0BED9, 1px -1px #B0BED9, -1px 1px #B0BED9;
}

table.dataTable tbody td.select-checkbox:before, table.dataTable tbody th.select-checkbox:before {
    content: ' ';
    margin-top: 2px;
    margin-left: -6px;
    border: 1px solid black;
    border-radius: 3px;
}


    </style>
</head>
<body>
 
    @if(Auth::check())
    @include('layouts.sidebar')
    @include('layouts.header')

    <div class="container">@include('flash::message')</div>
        
    <div class="br-mainpanel">
     
       <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">@yield('title')</h4>
        <p class="mg-b-0">@yield('subtitle')</p>
      </div>

      <div class="br-pagebody">
            
            @yield('content')
        
      </div>
       
      <!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; {{ date('Y') }}. Angelo CRM. All Rights Reserved.</div>
         
        </div>

      </footer>
    </div>
    @else
     @yield('content')
    @endif
    <!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

<script src="{{ asset('theme/js/jquery.min.js') }}"></script>
<script src="{{ asset('theme/js/datepicker.js') }}"></script>
<script src="{{ asset('theme/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('theme/js/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('theme/js/moment.min.js') }}"></script>
<script src="{{ asset('theme/js/jquery.peity.min.js') }}"></script>
<script src="{{ asset('theme/js/d3.min.js') }}"></script>
<script src="{{ asset('theme/js/d3.layout.min.js') }}"></script>
<script src="{{ asset('theme/js/rickshaw.min.js') }}"></script>
<script src="{{ asset('theme/js/jquery.flot.js') }}"></script>
<script src="{{ asset('theme/js/jquery.flot.js') }}"></script>
<script src="{{ asset('theme/js/jquery.flot.spline.min.js') }}"></script>
<script src="{{ asset('theme/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('theme/js/echarts.min.js') }}"></script>
<script src="{{ asset('theme/js/select2.full.min.js') }}"></script>
<script src="{{ asset('theme/js/select2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/styles/metro/notify-metro.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>

<script src="{{ asset('theme/js/bracket.js') }}"></script>
<script src="{{ asset('theme/js/map.shiftworker.js') }}"></script>
<script src="{{ asset('theme/js/ResizeSensor.js') }}"></script>
<script src="{{ asset('theme/js/dashboard.js') }}"></script>


<script>
    $('#flash-overlay-modal').modal();
    $('.selectize').selectize({
        plugins: ['remove_button'],
        
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $( document ).ajaxStart(function() {
        
    });

    $( document ).ajaxComplete(function() {
        
    });

   $('.deleteFrm').on('submit', function () {

    var title = $(this).find('.btn-delete').attr('data-title');

    var poll = confirm("Confirm Delete "+title+" ?");

    if(poll)
    {
        return true;
    }
    else
    {
        return false;
    }

})
</script>

@stack('scripts')
</body>
</html>
