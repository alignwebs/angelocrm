@extends('layouts.app')
@section('title', $title)
@section('content')
<style>
	.table {font-size: 12px;}
</style>


	<div class="text-right">
		<a href="javascript:void(0)" class="btn btn-info" id="addNetwork">+ Add Network</a>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-12">

			<div class="card shadow-base bd-0">
			    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
			      	<h6 class="card-title tx-uppercase tx-12 mg-b-0">Network List</h6>
			    </div><!-- card-header -->
			    <div class="card-body justify-content-between align-items-center">
			  @include('flash::message')
			 <div id="success"></div>
			  <div id="result">
			  		
			  </div>
			 </div>
			</div>
		</div>
	</div>
<!--   	<br>
  	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Manage Network <button id="addNetwork" class="btn btn-success btn-sm pull-right" style="margin-top: -4px;">Add Network</button><br></div>
                
                <div class="panel-body">
                	
                		@include('flash::message')
                		<div id="success"></div>
                		<div id="result">
							

						</div>



                
                </div>
            </div>
        </div>


    </div> -->


<!-- add Modal -->
<div class="modal fade" id="NetworkModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Network</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="errors"></div>
        {!! Form::open(['route'=>'network.store','class'=>'form-material','id'=>'addNetworkForm']) !!}
        <div class="row">
        	<div class="col-md-6">
		        <div class="form-group">
		          <label>Network Platform</label>
		          {!! Form::select('network_platform_id',$network_platform,null,['class'=>'form-control form-control-line']) !!}
		        </div>
    		</div>
    		<div class="col-md-6">
		       <div class="form-group">
		            <label>Name</label>
		            {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
		        </div>
    		</div>
       </div>
       <div class="row">
        	<div class="col-md-6">
		        <div class="form-group">
		          <label>Api Key</label>
		          {!! Form::text('api_key',null,['class'=>'form-control form-control-line']) !!}
		        </div>
    		</div>
    		<div class="col-md-6">
		      <div class="form-group">
		           <label>Affiliate ID</label>
		           {!! Form::text('affiliate_id',null,['class'=>'form-control form-control-line']) !!}
		      </div>
    		</div>
       </div>
        <div class="row">
        	<div class="col-md-6">
		        <div class="form-group">
		          <label>Domain</label>
		          {!! Form::text('domain',null,['class'=>'form-control form-control-line']) !!}
		        </div>
    		</div>
    		<div class="col-md-6">
		        <div class="form-group">
		          <label>Timezone</label>
		          {!! Form::select('timezone',timezone(),null,['class'=>'form-control form-control-line']) !!}
		        </div>
    		</div>
       </div>
       <div class="row">
	       <div class="col-md-6">
	         <div class="form-group">
	          <label>Active</label>
	          {!! Form::radio('active',1) !!}
	          <label>Inactive</label>
	          {!! Form::radio('active',0) !!}
	        </div> 
	        </div>
     	</div>


      </div>
      <div class="modal-footer">
        <input type="submit" value="Submit" class="btn btn-info">

      </div>

      {!! Form::close() !!}
      <!-- Modal footer -->


    </div>
  </div>
</div>


<!-- Edit Modal -->
<div class="modal fade" id="editNetworkModal">
  <div class="modal-dialog">
    <div class="modal-content" id="editResult">



    </div>
  </div>
</div>
@endsection




@push('scripts')

<script>
	  
	  $(document).ready(function(){

	    getNetwork();
	    $("#addNetwork").click(function(){
	      $("#errors").text("");
	      $("#NetworkModal").modal();
	    });






	  $('#addNetworkForm').submit(function (data) {


	  	$.post("{{ route('network.store') }}",$(this).serialize(), function(data){


	      if(data.errors)
	      {
	        $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

	      }else{

	        if(data.success == 'true')
	        {
	          $("#NetworkModal").modal('hide');
	          getNetwork();
	          $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
	          $('#addNetworkForm')[0].reset();

	        }else{

	          $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

	        }

	      }


	      })

	  		return false;

	  });

	  });

	  function getNetwork()
	  {
	    $("#result").html("Loading...");
	    $.get('{{ route("network.list") }}',function(data){

	     $("#result").html(data);

	     $('#networkTable').DataTable({
	     			responsive: true,
	     	          language: {
	     	            searchPlaceholder: 'Search...',
	     	            sSearch: '',
	     	            
	     	          }
	     });

	     deleteConfirm();

	   });
	  }

	  function edit(id)
	  {
	   $("#editNetworkModal").modal();

	   $.get('{{ route("edit.network") }}',{id:id},function(data){
	     $("#editResult").html(data);
	     $('#networkTable').DataTable();
	   });

	 }

	 function deleteConfirm()
	 {
	  $('.deleteFrm').on('submit', function () {

	    var title = $(this).find('.btn-delete').attr('data-title');

	    var poll = confirm("Confirm Delete "+title+" ?");

	    if(poll)
	    {
	      return true;
	    }
	    else
	    {
	      return false;
	    }

	  })
	}

</script>

@endpush