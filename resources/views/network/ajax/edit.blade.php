    {!! Form::model($network, ['route' => ['update.network', $network->id],'id'=>'editNetworkForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Network</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors2"></div>

        {!! Form::hidden('id',$network->id) !!}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Network Platform</label>
              {!! Form::select('network_platform_id',$network_platform,null,['class'=>'form-control form-control-line']) !!}
            </div>
        </div>
        <div class="col-md-6">
           <div class="form-group">
                <label>Name</label>
                {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
            </div>
        </div>
       </div>
       <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Api Key</label>
              {!! Form::text('api_key',null,['class'=>'form-control form-control-line']) !!}
            </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
               <label>Affiliate ID</label>
               {!! Form::text('affiliate_id',null,['class'=>'form-control form-control-line']) !!}
          </div>
        </div>
       </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Domain</label>
              {!! Form::text('domain',null,['class'=>'form-control form-control-line']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
              <label>Timezone</label>
              {!! Form::select('timezone',timezone(),null,['class'=>'form-control form-control-line']) !!}
            </div>
        </div>
       </div>
      <div class="row">
               <div class="col-md-6">
                 <div class="form-group">
                  <label>Active</label>
                  {!! Form::radio('active',1,($network->active) ? true:false) !!}
                  <label>Inactive</label>
                  {!! Form::radio('active',0,!($network->active) ? true:false) !!}
                </div> 
                </div>
        </div>
       
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-info">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editNetworkForm').submit(function () { 

        $.post("{{ route('update.network') }}",$(this).serialize(), function(data){


            if(data.errors)
            {
              $("#errors2").html('<div class="alert alert-danger">'+data.errors+'</div>');

            }else{

              if(data.success == 'true')
              {
                $("#editNetworkModal").modal('hide');
                getNetwork();
                $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                $('#editNetworkForm')[0].reset();

              }else{

                $("#errors2").html('<div class="alert alert-danger">Something went wrong</div>');

              }

            }

        });




        return false;

      });

    </script>
