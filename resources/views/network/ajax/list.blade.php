
<table id="networkTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
          <th>#</th>
          <th>Network PlatForm</th>
          <th>Name</th>
          <th>Api Key</th>
          <th>Affiliate ID</th>
          <th>Domain</th>
          <th>Status</th>
          <th>Timezone</th>
        
          <th>Manage</th>
      </tr>
  </thead>

    <tbody>

     @foreach($rows as $row)
    <tr>
        <td>{{ $row->id }}</td>
        <td>{{ $row->network_platform->platform_name }}</td>
        <td>{{ $row->name }}</td>
        <td>{{ $row->api_key }}</td>
        <td>{{ $row->affiliate_id }}</td>
        <td>{{ $row->domain }}</td>
        <td>
          {{ ($row->active) ? 'Active' : 'Inactive' }}
        </td>
        <td>{{ $row->timezone }}</td>
        <td>
          {!! ModelBtn2('network', $row->id) !!}
         </td>
    </tr>
     @endforeach
      
    </tbody>
  </table>
