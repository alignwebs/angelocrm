@extends('master')

@section('content')


<style>
    #users-table { font-size:12px;   }
</style>
    <table class="table table-bordered table-condensed" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>visitor_id</th>
                <th>hit_id</th>
                <th>offer_id</th>
                <th>s1</th>
                <th>s2</th>
                <th>s3</th>
                <th>s4</th>
                <th>s5</th>
                <th>listname</th>
                <th>date</th>
                <th>spot</th>
                <th>source</th>
                <th>type</th>
            </tr>
        </thead>
        

        <tfoot>
             <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>visitor_id</th>
                <th>hit_id</th>
                <th>offer_id</th>
                <th>s1</th>
                <th>s2</th>
                <th>s3</th>
                <th>s4</th>
                <th>s5</th>
                <th>listname</th>
                <th>date</th>
                <th>spot</th>
                <th>source</th>
                <th>type</th>
        </tfoot>
    </table>


@stop

@push('scripts')
<script>
 $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('get-data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'visitor_id', name: 'visitor_id' },
            { data: 'hit_id', name: 'hit_id' },
            { data: 'offer_id', name: 'offer_id' },
            { data: 's1', name: 's1' },
            { data: 's2', name: 's2' },
            { data: 's3', name: 's3' },
            { data: 's4', name: 's4' },
            { data: 's5', name: 's5' },
            { data: 'listname', name: 'listname' },
            { data: 'date', name: 'date' },
            { data: 'spot', name: 'spot' },
            { data: 'source', name: 'source' },
            { data: 'type', name: 'type' }
        ],
        initComplete: function () {

            this.api().columns().every(function () {

                var column = this;
                var input = document.createElement("input");
               
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        }
    });
</script>
@endpush