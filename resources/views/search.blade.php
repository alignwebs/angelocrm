@extends('layouts.app')
@section('title', $title)
@section('content')
<style>
    .filter-container { max-width: 900px; margin: 0 auto; }
	.filter-container .row { margin-bottom: 10px;  }
	#result { display: none; }
	.dataTables_filter { float: right; margin-top: 15px;}
	#users-table, #users-table-all { font-size:12px;   }
	.table-rows-action-btns {float: left;margin-top: -50px;}
	.table-rows-action-btns .btn-group {margin-bottom: 20px;}
	/*.card-body { overflow-x: scroll; }*/
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow-base bd-0">
            <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                <h6 class="card-title tx-uppercase tx-12 mg-b-0">
                    Fliter Search
                </h6>
            </div>
            <!-- card-header -->
            <div class="card-body justify-content-between align-items-center">
                <div class="filter-container">
                    <form action="{{ route('do-search') }}" id="filterfrm" method="GET">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
            <!-- card-body -->
            <div class="card-footer text-center">
                <button class="btn btn-block btn-primary" onclick="$('#filterfrm').submit()" style="max-width: 800px; margin: 0 auto">
                    Search
                </button>
            </div>
        </div>
    </div>
</div>
<br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-base bd-0">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                    <h6 class="card-title tx-uppercase tx-12 mg-b-0">
                        Lead List
                    </h6>
                </div>
                <!-- card-header -->
                <div class="card-body justify-content-between align-items-center">
                    <table class="table table-bordered table-condensed table-hover" id="users-table-all" style="width: 100%;">
                        <thead>
                            <tr class="active">
                                <th>
                                </th>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Mobile
                                </th>
                                <th>
                                    visitor_id
                                </th>
                                <th>
                                    hit_id
                                </th>
                                <th>
                                    offer_id
                                </th>
                                <th>
                                    offer_name
                                </th>
                                <th>
                                    s1
                                </th>
                                <th>
                                    s2
                                </th>
                                <th>
                                    s3
                                </th>
                                <th>
                                    s4
                                </th>
                                <th>
                                    s5
                                </th>
                                <th>
                                    listname
                                </th>
                                <th>
                                    date
                                </th>
                                <th>
                                    spot
                                </th>
                                <th>
                                    source
                                </th>
                                <th>
                                    type
                                </th>
                                <th>
                                    aw_status
                                </th>
                                <th>
                                    aw_log
                                </th>
                                <th>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div id="result">
                        <table class="table table-bordered table-condensed table-hover" id="users-table" style="width: 100%;">
                            <thead>
                                <tr class="active">
                                    <th>
                                    </th>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        {{ tableHeader('name') }}
                                    </th>
                                    <th>
                                        {{ tableHeader('email') }}
                                    </th>
                                    <th>
                                        {{ tableHeader('mobile') }}
                                    </th>
                                    <th>
                                        {{ tableHeader('flux_visitor') }}
                                    </th>
                                    <th>
                                        {{ tableHeader('flux_hid') }}
                                    </th>
                                    <th>
                                        {{ tableHeader('offer_id') }}
                                    </th>
                                    <th>
                                        {{ tableHeader('offer_name') }}
                                    </th>
                                    <th>
                                        s1
                                    </th>
                                    <th>
                                        s2
                                    </th>
                                    <th>
                                        s3
                                    </th>
                                    <th>
                                        s4
                                    </th>
                                    <th>
                                        s5
                                    </th>
                                    <th>
                                        listname
                                    </th>
                                    <th>
                                        date
                                    </th>
                                    <th>
                                        spot
                                    </th>
                                    <th>
                                        source
                                    </th>
                                    <th>
                                        type
                                    </th>
                                    <th>
                                        aw_status
                                    </th>
                                    <th>
                                        aw_log
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-rows-action-btns">
                        <div aria-label="..." class="btn-group" role="group">
                            <button class="btn btn-default btn-edit" disabled="disabled" onclick="editRow()" type="button">
                                <i class="fa fa-edit">
                                </i>
                                Edit
                            </button>
                            <button class="btn btn-default btn-delete" disabled="disabled" onclick="deleteRows()" type="button">
                                <i class="fa fa-trash">
                                </i>
                                Delete
                            </button>
                        </div>
                        <div class="stats">
                            <span class="selected-rows-all">
                            </span>
                            <span>
                                <a href="javascript:void(0)" onclick="clearAll()">
                                    clear all
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- card-body -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Deleting Records...
                    </h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-close refresh" data-dismiss="modal" style="display: none;" type="button">
                        Close
                    </button>
                    <div class="progress" style="margin-bottom: 0">
                        <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="45" class="progress-bar progress-bar-striped active" role="progressbar" style="width:100%">
                            <span class="sr-only">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="modal fade" id="edit-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Edit Lead
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" onclick="$('#edit-modal form').submit()" type="button">
                        Update
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    @endsection




@push('scripts')
    <script>
        $(document).ready(function($) {
			addFilter();
			getAll();
	});	
	
	var filters = {!! json_encode($filters) !!};
	var coulmns = {!! json_encode($coulmns) !!};
	var selected = [];
	var table;
    </script>
    {{-- ******************************** FILTER FUNCTIONS ***************************************** --}}
@include('leads.js.filter')	

{{-- ******************************** ALL LEADS FUNCTIONS ***************************************** --}}
@include('leads.js.getall')	

{{-- ******************************** COMMON FUNCTIONS ***************************************** --}}
    <script>
        function reinit()
	{
		$('.select-checkbox').text(null);

		if(selected.length > 0)
		{
			$('.btn-delete').removeAttr('disabled');

			if(selected.length > 1)
				$('.btn-edit').attr('disabled','disabled');
			else
				$('.btn-edit').removeAttr('disabled');
		}
		else
		{
			$('.btn-edit, .btn-delete').attr('disabled','disabled');
		}


		$('.selected-rows-all').html("Total Rows Selected: "+selected.length)

	}

	function clearAll()
	{
		selected = [];
		table.rows('.selected').deselect();
		reinit();
	}


	function deleteRows()
	{
		if(selected.length > 0)
		{
			var poll = confirm("Are you sure to delete "+selected.length+" selected rows ?");

			if(poll)
			{
				if($('#users-table-all').is(':visible'))
				{
					$('.modal-footer .btn-close').attr('onclick','window.location.reload()');
				}
				else
				{
					$('.modal-footer .btn-close').removeAttr('onclick');
				}

				$('#delete-modal .modal-body').html("");
				$('#delete-modal').modal({
				  keyboard: false,
				  backdrop: 'static'
				},'show');

				$.ajax({
					url: "{{ route('delete-leads') }}",
					method: "POST",
					data: {'ids[]':selected},
					xhrFields: {
						onprogress: function(e) {
							$('#delete-modal .modal-body').html(e.target.responseText)
							if (e.lengthComputable) {
								console.log(e.loaded / e.total * 100 + '%');
							}
						}
					},
					success: function(text) {
							
							//$('#sync-modal .modal-body').html(text+"<h1>done!</h1>")
							$('#delete-modal .progress').hide();
							$('#delete-modal .refresh').show();
							//alert("Sync Completed!")

							$('tr.selected').each(function () {
					
								$(this).find('td.select-checkbox').removeClass('select-checkbox');	
							})

							clearAll()

					}
				});

				
			}
		}
	}

	function editRow(id)
	{
		var id = selected[0];
		if(id > 0)
		{
			$('#edit-modal .modal-body').html("Loading...")

			$('#edit-modal .modal-body').load('{{ route('edit-leads') }}?id='+id);

			$('#edit-modal').modal('show');
		}
	}

	$(document).on( 'click', '.editRow', function () {
	           
               let id = $(this).data('id');

				if(id > 0)
				{
					$('#edit-modal .modal-body').html("Loading...")

					$('#edit-modal .modal-body').load('{{ route('edit-leads') }}?id='+id);

					$('#edit-modal').modal('show');
				}

    	});
    </script>
    @endpush
</br>