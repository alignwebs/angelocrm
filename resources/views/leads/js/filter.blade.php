<script>
function addFilter()
	{
		var html = "";
		var options = "";

		options += '<option value="">Search Criteria</option>';

		$.each(coulmns, function(key, value) {   

			options += '<option value="'+value+'">'+value+'</option>';

		});

		html += '<div class="col-3"><select name="column[]" class="form-control input-sm">'+options+'</select></div>';
		
		var options = "";

		$.each(filters, function(key, value) {   

			options += '<option value="'+value+'">'+value+'</option>';

		});

		html += '<div class="col-3"><select name="filter[]" class="form-control input-sm">'+options+'</select></div>';


		html += '<div class="col-4"><input class="form-control input-sm filter-value" name="value[]" required placeholder="Value"></div>';

		html += '<div class="col-2 text-right"><a href="javascript:void(0)" onclick="handleAddClick()" class="btn  btn-primary"><span class="fa fa-plus" aria-hidden="true"></span></a> <a href="javascript:void(0)" onclick="handleRemoveClick(this)" class="btn  btn-secondary"><span class="fa fa-remove" aria-hidden="true"></span></a></div>';
		//html += '<input name="end[]" type="hidden">';
		html = '<div class="row">'+html+'</div>';
		$(html).appendTo('.filter-container form')
	}


	function handleAddClick()
	{
		addFilter()
	}

	function handleRemoveClick(e)
	{
		if($('.filter-container .row').length > 1)
			$(e).parent().parent().remove();
	}

	$('#filterfrm').submit(function () {

		var fields = $(this).serialize();
		var action = $(this).attr('action');

		var check = 1;

		$(this).find('.filter-value').each(function(index, el) {
			if($(el).val() == "")
			{
				alert("Enter all the values")
				$(el).focus()
				check = 0;

				return false;
			}
			else
				check = 1;
		});

		
		if(check > 0)
		{

			
				$('#users-table').DataTable().destroy();

			$('#users-table tbody').html('')

			$('#result').hide();

			$.getJSON(action,fields, function (json) {

				$.each(json, function(index, key) {
					$('<tr><td></td><td>'+key.id+'</td><td>'+key.name+'</td><td>'+key.email+'</td><td>'+key.mobile+'</td><td>'+key.visitor_id+'</td><td>'+key.hit_id+'</td><td>'+key.offer_id+'</td><td>'+key.offer_name+'</td><td>'+key.s1+'</td><td>'+key.s2+'</td><td>'+key.s3+'</td><td>'+key.s4+'</td><td>'+key.s5+'</td><td>'+key.listname+'</td><td>'+key.date+'</td><td>'+key.spot+'</td><td>'+key.source+'</td><td>'+key.type+'</td><td>'+key.aweber_log+'</td><td>'+key.aweber_status+'</td><td></td></tr>').appendTo('#users-table tbody');
					
				});

				table = $('#users-table').DataTable({
					bLengthChange: false,
					columnDefs: [ {
			            orderable: false,
			            className: 'select-checkbox',
			            searchable: false,
			            targets:   0,
			            width: "20px",
			        },
			        {
			            targets: -1,
			            data: null,
			            searchable: false,
			            orderable: false,
			            defaultContent: '<button class="btn btn-sm btn-primary editRow"><i class="fa fa-edit"></i></button>'
			        }

	         ],
			        select: {
			            style:    'multi',
			            selector: 'td:first-child'
			        },

        			order: [[ 1, 'asc' ]],
			        dom: 'Bfrtip',
			    

		    language: {
		        buttons: {
		            selectAll: "Select all items",
		            selectNone: "Select none",
		            colvis: "Show Columns",
		        }
		    },
			 	
			        buttons: [
			            'pageLength', 'selectAll', 'selectNone','colvis',

			            { 
			            	extend: 'csv', 
			            	text: 'Save as CSV',
			            }
			        ]
			    });

				/* Hide Columns By Default */
			    table.columns( [4,5,6,7,13,14,15,16,17,18,19] ).visible( false, false );

			// On Table Row Select

			    table.on( 'select', function ( e, dt, type, indexes ) {
			    	
				    if ( type === 'row' ) {

				    	var data = table.rows( indexes ).data();
			 			var id = data[0][1];
			 			console.log(id)
				        var index = $.inArray(id, selected);
				 	
				        if ( index === -1 ) {
				            selected.push( id );
				        } else {
				            selected.splice( index, 1 );
				        }
		    		
				    }

				    reinit();
				} );

			    // On Table Row Deselect

				table.on( 'deselect', function ( e, dt, type, indexes ) {
			    
				    if ( type === 'row' ) {
				    	
				    	$.each(indexes, function (i) {
				    		var data = table.rows( i ).data().pluck( 'id' );
				 			var id = data[0];
					        var index = $.inArray(id, selected);
					 		selected.splice( index, 1 );	
				    	})
				        	
				    }

				     reinit();
				} );

				// On Table Initiation Complete
				table.on( 'draw', function () {
			        reinit();
			    })

				$('#users-table-all').DataTable().destroy();
				$('#users-table-all').remove();
				$('#result').show();


			})
			
		}


		return false;
	})

	</script>