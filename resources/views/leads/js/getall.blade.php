<script>
	
	function getAll()
	{
		table = $('#users-table-all').DataTable({
			// stateSave: true,
			scrollX:        true,
      scrollCollapse: false,
      
			columnDefs: [ {
		            orderable: false,
		            className: 'select-checkbox',
		            searchable: false,
		            targets:   0,
		            width: '30px'
	        },

	        {
	            targets: -1,
	            data: null,
	            searchable: false,
	            orderable: false,
	            render: function ( data, type, full, meta ) {
                     return '<button class="btn  btn-sm btn-primary editRow" data-id="'+full.id+'"><i class="fa fa-edit"></i></button>';
                 }
	            
	        }

	        ],
	         select: {
	            style:    'multi',
	            selector: 'td:first-child'
	        },
	        order: [[ 1, 'desc' ]],
					
					dom: 'Bfrtip',
	        "rowCallback": function( row, data ) {
	            if ( $.inArray(data.id, selected) !== -1 ) {
	                $(row).addClass('selected');
	            }
	        },
	        buttons: [
	        	'pageLength',
		        'selectAll',
		        'selectNone',
		        'colvis'
		    ],

		    language: {
		        buttons: {
		            selectAll: "Select all items",
		            selectNone: "Select none",
		            colvis: "Show Columns",
		        },
		        searchPlaceholder: 'Search...',
		        sSearch: ''
		    },
	        processing: true,
	        serverSide: true,
	        ajax: '{!! route('allData') !!}',

	        columns: [
	         	{ data: null, name: null },
	         	{ data: 'id', name: 'id' },
	            { data: 'name', name: 'name' },
	            { data: 'email', name: 'email' },
	            { data: 'mobile', name: 'mobile' },
	            { data: 'visitor_id', name: 'visitor_id' },
	            { data: 'hit_id', name: 'hit_id' },
	            { data: 'offer_id', name: 'offer_id' },
	            { data: 'offer_name', name: 'offer_name' },
	            { data: 's1', name: 's1' },
	            { data: 's2', name: 's2' },
	            { data: 's3', name: 's3' },
	            { data: 's4', name: 's4' },
	            { data: 's5', name: 's5' },
	            { data: 'listname', name: 'listname' },
	            { data: 'date', name: 'date' },
	            { data: 'spot', name: 'spot' },
	            { data: 'source', name: 'source' },
	            { data: 'type', name: 'type' },
	            { data: 'aweber_log', name: 'aweber_log' },
	            { data: 'aweber_status', name: 'aweber_status' },
	            { data: null, name: null },

        	]
	    });

		/* Hide Columns By Default */
	    //table.columns( [5,6,7,13,14,15,16,17,18,19,20] ).visible( false, false );

		// On Table Row Select

	    table.on( 'select', function ( e, dt, type, indexes ) {
	    	
		    if ( type === 'row' ) {

		    	if(indexes.length > 1)
		    	{
		    		console.log('multiple')
		    		$.each(indexes, function (i) {
			    		var data = table.rows( i ).data().pluck( 'id' );
			 			var id = data[0];
				        var index = $.inArray(id, selected);
				 
				        if ( index === -1 ) {
				            selected.push( id );
				        } 
			    	})
		    	}
		    	else
		    	{
		    		console.log('single')
		    			var data = table.rows( indexes ).data().pluck( 'id' );
			 			var id = data[0];
				        var index = $.inArray(id, selected);
				 	
				        if ( index === -1 ) {
				            selected.push( id );
				        } else {
				            selected.splice( index, 1 );
				        }
		    	}
		    	
		    	
		        	
		    }

		    reinit();
		} );

	    // On Table Row Deselect

		table.on( 'deselect', function ( e, dt, type, indexes ) {
	    
		    if ( type === 'row' ) {
		    	
		    	$.each(indexes, function (i) {
		    		var data = table.rows( i ).data().pluck( 'id' );
		 			var id = data[0];
			        var index = $.inArray(id, selected);
			 		selected.splice( index, 1 );	
		    	})
		        	
		    }

		     reinit();
		} );

		// On Table Initiation Complete
		table.on( 'draw', function () {
	        reinit();
	    })
	
}
</script>