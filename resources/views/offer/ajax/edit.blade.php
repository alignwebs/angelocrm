
<form action="{{ $form_action }}" method="POST" id="offer-form">
	<input type="hidden" name="id" value="{{ $row->id }}">
	<div class="form-group">
		<label for="tags">Flux Page ID:</label>
		{!! Form::number('flux_page_id',$row->flux_page_id,['class'=>'form-control']) !!}
	</div>
	<div class="form-group">
			<label for="tags">Offer Url:</label>
			{!! Form::text('offer_url',$row->offer_url,['class'=>'form-control']) !!}
	</div>
	<div class="form-group">
		<label for="tags">Offer Redirect Domain:</label>
		{!! Form::text('offer_redirect_domain',$row->offer_redirect_domain,['class'=>'form-control']) !!}
	</div>
</form>

<script>
	$('#offer-form').submit(function () {

		var action = $(this).attr('action');
		var fields = $(this).serialize();

		$.post(action, fields, function (data) {
			$('button.close').click();
			$.notify(data, "success");
			getAll();
		})

		return false;
	})

</script>