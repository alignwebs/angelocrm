@extends('layouts.app')
@section('title', $title)
@section('content')
<style>
	.table {font-size: 12px;}
</style>

<div class="page-action-btns">
	<ul class="list-inline">
		<li class="list-inline-item"><button class="btn btn-info" onclick="sync('EverflowAccounts')"><span class="fa fa-refresh"></span>&nbsp;Sync: Everflow Accounts</button></li>
		<li class="list-inline-item"><button class="btn btn-info" onclick="sync('hasOffersAccounts')"><span class="fa fa-refresh"></span>&nbsp;Sync: HasOffers Accounts</button></li>
		<li class="list-inline-item"><button class="btn btn-info" onclick="sync('cakesAccounts')"><span class="fa fa-refresh"></span>&nbsp;Sync: Cake Accounts</button></li>
		<li class="list-inline-item"><button class="btn btn-info" onclick="sync('hasOffersStatus')"><span class="fa fa-refresh"></span>&nbsp;Sync: HasOffers Status</button></li>
		<li class="list-inline-item"><button class="btn btn-info" onclick="sync('cakeStatus')"><span class="fa fa-refresh"></span>&nbsp;Sync: Cake Status</button></li>
	</ul>
</div>

	<div class="row">
        <div class="col-lg-12">
  			<div class="card shadow-base bd-0">
  			    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
  			      	<h6 class="card-title tx-uppercase tx-12 mg-b-0">Offers List</h6>
  			    </div><!-- card-header -->
  			    <div class="card-body justify-content-between align-items-center">
                	

                		<div id="result">
							
						    <table class="table table-bordered table-condensed table-hover" id="offerstable" style="width: 100%;">
						        <thead>
					
						            <tr>
						            	
						                <th>#</th>
						                <th>{{ tableHeader('Offer Id') }}</th>
						                <th>{{ tableHeader('Flux PageID') }}</th>
						                <th>{{ tableHeader('Offer Name') }}</th>
						                <th>{{ tableHeader('Price') }}</th>
						                <th>{{ tableHeader('Platform') }}</th>
						                <th>{{ tableHeader('Network') }}</th>
						                <th>{{ tableHeader('Offer Status') }}</th>
														<th></th>
												</tr>
						        </thead>
						        
						        <tbody>
						       
						        </tbody>

						   
						    </table>
						</div>



                	</div>
                </div>
            </div>

    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Offer</h4>
          	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
           
          </div>
          <div class="modal-footer text-right">
           	<button type="button" class="btn btn-primary" onclick="$('#edit-modal form').submit()">Update</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



@endsection




@push('scripts')

<script>
	var selected = [];

	var datatable;
	$(document).ready(function() {
			getAll();
	});


	function getAll()
	{
				if(datatable)
					datatable.destroy();

				datatable = $('#offerstable').DataTable({
					stateSave: true,
					searchDelay: 1000,
					responsive: true,
			          language: {
			            searchPlaceholder: 'Search...',
			            sSearch: '',
			            
			          },
			        columnDefs: [

			        {
			            targets: -1,
			            data: null,
			            searchable: false,
			            orderable: false,
			            defaultContent: '<button class="btn btn-sm btn-primary editRow"><i class="fa fa-edit"></i></button>'
			        }

			        ],
			        processing: true,
			        serverSide: true,
			        ajax: '{!! route('offers-allData') !!}',

			        columns: [
			         	
			         	{ data: 'id', name: 'id' },
			            { data: 'offer_id', name: 'offer_id' },
			            { data: 'flux_page_id', name: 'flux_page_id' },
			            { data: 'offer_name', name: 'offer_name' },
			            { data: 'price', name: 'price' },
			            { data: 'network_platform_name', name: 'network_platform_name', searchable: false, orderable: false  },
			            { data: 'network_name', name: 'network_name', searchable: false, orderable: false },
			            { data: 'offer_status', name: 'offer_status' },
			            { data: null, name: null },
			        
			          

		        	]
			    });

		}


		$('table tbody').on( 'click', '.editRow', function () {
		        var data = datatable.row( $(this).parents('tr') ).data();
				//console.log(data)
				var id = data['id'];

				if(!id)
					id = data[1];

					if(id > 0)
					{
						$('#edit-modal .modal-body').html("Loading...")

						$('#edit-modal .modal-body').load('{{ route('offers-edit') }}?id='+id);

						$('#edit-modal').modal({backdrop:'static'},'show');
					}

	    	});

    function checkResponse(response)
    {
    	if(response.success == 'true')
    		{
    			$.notify("Synced Successfully!", "success");
    			if(datatable)
					datatable.destroy();
		    	getAll();

    		}else{

    			$.notify('Something went wrong', "error");
    			
    		}

    	
    }		

	function sync(param)
	{	

		if (confirm('Are you sure you want to Sync?')) {
		    
		    switch (param) {

		        case 'hasOffersAccounts':

		            $.get('{{ route('sync-account-hasoffer') }}', function (response) { checkResponse(response) });

		            break;

		        case 'cakesAccounts':

		            $.get('{{ route('sync-account-cake') }}', function (response) { checkResponse(response) });

		            break;

		        case 'hasOffersStatus':

		            $.get('{{ route('sync-offers-hasoffer') }}', function (response) { checkResponse(response) });

		            break;

		        case 'cakeStatus':

		            $.get('{{ route('sync-offers-cake') }}', function (response) { checkResponse(response) });

		            break;

	            case 'EverflowAccounts':

	            $.get('{{ route('sync-account-everflow') }}', function (response) { checkResponse(response) });

	            break;

		        default: 
		            $.notify('Something went wrong', "error");
		    }


		}
	}

</script>

@endpush