@extends('layouts.app')
@section('title', $title)
@section('content')
<style>
	.table {font-size: 12px;}
</style>


  <div class="text-right">
  	<a href="javascript:void(0)" class="btn btn-info" id="addNetwork">+ Add Network Platform</a>
  </div>
  <br>
  <div class="row">
  	<div class="col-lg-12">

  		<div class="card shadow-base bd-0">
  		    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
  		      	<h6 class="card-title tx-uppercase tx-12 mg-b-0">Network Platform List</h6>
  		    </div><!-- card-header -->
  		    <div class="card-body justify-content-between align-items-center">
  		  @include('flash::message')
  		 <div id="success"></div>
  		  <div id="result">
  		  		
  		  </div>
  		 </div>
  		</div>
  	</div>
  </div>
<!--   	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Manage Network Platform <button id="addNetwork" class="btn btn-success btn-sm pull-right" style="margin-top: -4px;">Add Network Platform</button><br></div>
                
                <div class="panel-body">
                	
                		@include('flash::message')
                		<div id="success"></div>
                		<div id="result">
							

						</div>



                
                </div>
            </div>
        </div>


    </div> -->


<!-- add Modal -->
<div class="modal fade" id="NetworkPlatformModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Network Platform</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="errors"></div>
        {!! Form::open(['route'=>'platform.store','class'=>'form-material','id'=>'addNetworkPlatform']) !!}

        <div class="form-group">
          <label>Platform Name</label>
          {!! Form::text('platform_name',null,['class'=>'form-control form-control-line']) !!}
        </div>

      </div>
      <div class="modal-footer">
        <input type="submit" value="Submit" class="btn btn-info">

      </div>

      {!! Form::close() !!}
      <!-- Modal footer -->


    </div>
  </div>
</div>


<!-- Edit Modal -->
<div class="modal fade" id="editNetworkPlatformModal">
  <div class="modal-dialog">
    <div class="modal-content" id="editResult">



    </div>
  </div>
</div>
@endsection




@push('scripts')

<script>
	  
	  $(document).ready(function(){

	    getNetworkPlatform();
	    $("#addNetwork").click(function(){
	      $("#errors").text("");
	      $("#NetworkPlatformModal").modal();
	    });






	  $('#addNetworkPlatform').submit(function (data) {


	  	$.post("{{ route('platform.store') }}",$(this).serialize(), function(data){


	      if(data.errors)
	      {
	        $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

	      }else{

	        if(data.success == 'true')
	        {
	          $("#NetworkPlatformModal").modal('hide');
	          getNetworkPlatform();
	          $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
	          $('#addNetworkPlatform')[0].reset();

	        }else{

	          $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

	        }

	      }


	      })

	  		return false;

	  });

	  });

	  function getNetworkPlatform()
	  {
	    $("#result").html("Loading...");
	    $.get('{{ route("platform.list") }}',function(data){

	     $("#result").html(data);

	     $('#networkPlatformTable').DataTable({
	     	responsive: true,
			          language: {
			            searchPlaceholder: 'Search...',
			            sSearch: '',
			            
			          }

	     });

	     deleteConfirm();

	   });
	  }

	  function edit(id)
	  {
	   $("#editNetworkPlatformModal").modal();

	   $.get('{{ route("edit.platform") }}',{id:id},function(data){
	     $("#editResult").html(data);
	     $('#networkPlatformTable').DataTable();
	   });

	 }

	 function deleteConfirm()
	 {
	  $('.deleteFrm').on('submit', function () {

	    var title = $(this).find('.btn-delete').attr('data-title');

	    var poll = confirm("Confirm Delete "+title+" ?");

	    if(poll)
	    {
	      return true;
	    }
	    else
	    {
	      return false;
	    }

	  })
	}

</script>

@endpush