    {!! Form::model($platform, ['route' => ['update.platform', $platform->id],'id'=>'editPlatform','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Network Platform</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors2"></div>

        {!! Form::hidden('id',$platform->id) !!}

          <div class="form-group">
            <label>Platform Name</label>
            {!! Form::text('platform_name',null,['class'=>'form-control form-control-line']) !!}
          </div>
       
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-info">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editPlatform').submit(function () { 

        $.post("{{ route('update.platform') }}",$(this).serialize(), function(data){


            if(data.errors)
            {
              $("#errors2").html('<div class="alert alert-danger">'+data.errors+'</div>');

            }else{

              if(data.success == 'true')
              {
                $("#editNetworkPlatformModal").modal('hide');
                getNetworkPlatform();
                $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                $('#editPlatform')[0].reset();

              }else{

                $("#errors2").html('<div class="alert alert-danger">Something went wrong</div>');

              }

            }

        });




        return false;

      });

    </script>
