
<table id="networkPlatformTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
          <th>#</th>
          <th>Name</th>
        
          <th>Manage</th>
      </tr>
  </thead>

    <tbody>

     @foreach($rows as $row)
    <tr>
        <td>{{ $row->id }}</td>
        <td>{{ $row->platform_name }}</td>
        <td>
          {!! ModelBtn2('platform', $row->id) !!}
         </td>
    </tr>
     @endforeach
      
    </tbody>
  </table>
