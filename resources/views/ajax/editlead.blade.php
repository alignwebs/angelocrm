<style>
	
</style>

<form action="{{ $form_action }}" method="POST" id="update-lead-form">

<table class="table table-condensed table-hover" width="100%">


	<tr>
		<td><label for="tags">Tags:</label></td>
		<td>
			{!! Form::select('tags[]', $tags, json_decode($row['tags'], true), ['class'=>'selectize','multiple' => 'multiple']) !!}
		</td>
	</tr>

	<tr style="display: none;">
		<td><label for="tags">Old Tags:</label></td>
		<td>
			{!! Form::select('aweber_tags[]', $aweber_tags, json_decode($row['tags'], true), ['multiple' => 'multiple']) !!}
		</td>
	</tr>


@foreach($row as $key=>$value)
	
	@if($key == 'id')
		<input type="hidden" name="{{ $key }}" value="{{ $value }}">
	
	@elseif($key == 'tags')
		
	@else
	
		<tr>
			<td><label for="{{ $key }}">{{ tableHeader(format_text($key)) }}</label></td>
			<td><input class="form-control input-sm" type="text" name="{{ $key }}" value="{{ $value }}"></td>
		</tr>
		
	@endif

@endforeach

</table>


</form>


<script>
	$('#update-lead-form').submit(function () {

		var action = $(this).attr('action');
		var fields = $(this).serialize();

		$.post(action, fields, function (data) {
			$('button.close').click();
			$.notify(data, "success");
		})

		return false;
	})

 $('.selectize').selectize({
 	plugins: ['remove_button'],

 });
</script>