					{!! Form::model($row, ['method'=>'PUT','route' => ['offer-condition.update', $row->id],'id'=>"edit-offer-condition"]) !!}
					
					
					<table class="table table-condensed" width="100%">


						<tr>
							<td class="td_label"><label for="offer_id">Offer ID:</label></td>
							<td>
								{!! Form::text('offer_id', null,['id'=>'offer_id','class' => 'form-control','required' => 'required', 'placeholder'=>'Enter Offer ID']); !!}
							</td>
						</tr>
						<tr>
							<td class="td_label"><label for="network_platform">Network Platform: </label></td>
							<td>
								@foreach($networks as $key => $type)

								{{ $key }} {!! Form::radio('network_platform', $key,false) !!}


								@endforeach
							</td>
						</tr>
						
						@foreach($networks as $key => $type)

						<tr style="display: none" class="type {{ $key }}"">
							<td class="td_label"><label for="type">Type: </label></td>
							<td>
								{!! Form::select('type', array_combine($type,$type), null, ['placeholder' => 'Select Type','class'=>'form-control select_type','id'=>$key]); !!}
							</td>
						</tr>
						@endforeach
					
						<tr>
							<td class="td_label"><label for="url">Url:</label></td>
							<td>
								{!! Form::textarea('url', null,['id'=>'url','class' => 'form-control','required' => 'required', 'placeholder'=>'Enter Url','rows'=>4]); !!}
							</td>
						</tr>
						<tr>
							<td class="td_label"><label for="final_domain">Final Domain</label></td>
							<td>
								{!! Form::text('final_domain', null,['id'=>'final_domain','class' => 'form-control', 'required' => 'required','placeholder'=>'Enter Final Domain']); !!}
							</td>
						</tr>
					


					<tr>
						<td></td>
						<td><br><button type="submit" class="btn btn-primary">Update</button></td>
					</tr>
					</table>


					</form>


<script>
	$('#edit-offer-condition').submit(function () {

		var action = $(this).attr('action');
		var fields = $(this).serialize();

		$.post(action, fields, function (response) {



			if(response.errors){
				$.each(response.errors, function(k, v) {

					$("#errors").html('<div class="alert alert-danger">'+v+'</div>');
				});
			}else{

				$('button.close').click();
				$.notify("Updated Successful Offer ID "+response.offer_id+" of "+response.type, "success");
				getOfferCondition();
				
			}
		})

		return false;
	})

</script>
