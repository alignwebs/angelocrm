    {!! Form::model($user, ['route' => ['update.user', $user->id],'id'=>'editUserForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit User</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >

      <div id="errors2"></div>

      {!! Form::hidden('id',$user->id) !!}
    
      
            <div class="form-group">
                <label>Name </label>
                {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
              </div>
         
       
            <div class="form-group">
              <label>Email</label>
              {!! Form::text('email',null,['class'=>'form-control form-control-line']) !!}
            </div>
          
           <div class="row">
          <div class="col-md-6">
            <div class="form-group">
            <label>Password</label>
            {!! Form::password('password',['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
            <label>Confirm Password</label>
            
            {!! Form::password('password_confirmation',['class'=>'form-control form-control-line', 'id'=>"password-confirm"]) !!}
            </div>
          </div>
           <div class="col-md-6">
           <div class="form-group">
              <label>Role</label>

              {!! Form::select('role',getRoles(),$user->getRoleNames(),['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
           <div class="col-md-6">
          <div class="form-group" style="margin-top:40px">
              <label>Active</label>
              {!! Form::radio('active',1) !!}
              <label>Inactive</label>
              {!! Form::radio('active',0) !!}
            </div> 
          </div>

        </div>
       
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-info">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editUserForm').submit(function () { 

        $.post("{{ route('update.user') }}",$(this).serialize(), function(data){


            if(data.errors)
            {
              $("#errors2").html('<div class="alert alert-danger">'+data.errors+'</div>');

            }else{

              if(data.success == 'true')
              {
                $("#editUserModal").modal('hide');
                getUser();
                $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                $('#editUserForm')[0].reset();

              }else{

                $("#errors2").html('<div class="alert alert-danger">Something went wrong</div>');

              }

            }

        });




        return false;

      });

    </script>
