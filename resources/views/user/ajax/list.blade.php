
<table id="userTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
          <th>#</th>
          <th>Name</th>
          <th>Email</th>
          <th>Status</th>
          <th>Manage</th>
      </tr>
  </thead>

    <tbody>

     @foreach($rows as $row)
      @if($row->active == 1)
        @php
          $status = 'Active';
        @endphp
      @else
        @php
          $status = 'Inactive';
        @endphp
      @endif
    <tr>
        <td>{{ $row->id }}</td>
        <td>{{ $row->name }}</td>
        <td>{{ $row->email }}</td>
        <td>{{ $status }}</td>
        <td>
          {!! ModelBtn2('user', $row->id,$row->id) !!}
         </td>
    </tr>
     @endforeach
      
    </tbody>
  </table>
