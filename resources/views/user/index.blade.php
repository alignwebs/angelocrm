@extends('layouts.app')
@section('title', $title)
@section('content')
<style>
	.table {font-size: 12px;}
</style>


	<div class="text-right">
		<a href="javascript:void(0)" class="btn btn-info" id="addUser">+ Add User</a>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-12">

			<div class="card shadow-base bd-0">
			    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
			      	<h6 class="card-title tx-uppercase tx-12 mg-b-0">User List</h6>
			    </div><!-- card-header -->
			    <div class="card-body justify-content-between align-items-center">
			  @include('flash::message')
			 <div id="success"></div>
			  <div id="result">
			  		
			  </div>
			 </div>
			</div>
		</div>
	</div>

<!-- Edit Modal -->
<div class="modal fade" id="editUserModal">
  <div class="modal-dialog">
    <div class="modal-content" id="editResult">



    </div>
  </div>
</div>

<!-- add Modal -->
<div class="modal fade" id="UserModal">
  <div class="modal-dialog">
    <div class="modal-content">

      	{!! Form::open(['class'=>'form-material','id'=>'addUserForm']) !!}
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New User</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      	  <div id="errors"></div>
      	  <div class="row">
      	    <div class="col-md-12">
      	      <div class="form-group">
      	        <label>Name</label>
      	        {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
      	      </div>
      	    </div>
      	     
      	  <div class="col-md-12">
      	  <div class="form-group">
      	    <label>Email</label>
      	    {!! Form::text('email',null,['class'=>'form-control form-control-line','id'=>'email']) !!}
      	  </div>
      	</div>
      	  <div class="col-md-6">
      	  <div class="form-group">
      	    <label>Password</label>
      	    {!! Form::password('password',['class'=>'form-control form-control-line','id'=>'password']) !!}
      	  </div>
      	</div>
      	  <div class="col-md-6">
      	  <div class="form-group">
      	    <label>Confirm Password</label>
      	    
      	    {!! Form::password('password_confirmation',['class'=>'form-control form-control-line', 'id'=>"password-confirm"]) !!}
      	  </div>
      	</div>
      	  <div class="col-md-6">
      	  <div class="form-group">
      	    <label>Role</label>

      	    {!! Form::select('role',getRoles(),null,['class'=>'form-control form-control-line']) !!}
      	  </div>
      	</div>
      	  <div class="col-md-6">
      	   <div class="form-group" style="margin-top:40px">

      	    <label>Active</label>
      	    {!! Form::radio('active',1,true) !!}
      	    <label>Inactive</label>
      	    {!! Form::radio('active',0,null) !!}
      	  </div> 
      	  </div>
      	</div>
      <!-- Modal footer -->


    </div>
      <div class="modal-footer">
        <input type="submit" value="Submit" class="btn btn-info">
      </div>

      {!! Form::close() !!}
  </div>
</div>



@endsection




@push('scripts')

<script>
	  
	  $(document).ready(function(){

	    getUser();
	    $("#addUser").click(function(){
	      $("#errors").text("");
	      $("#UserModal").modal();
	    });






	  $('#addUserForm').submit(function (data) {


	  	$.post("{{ route('user.store') }}",$(this).serialize(), function(data){


	      if(data.errors)
	      {
	        $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

	      }else{

	        if(data.success == 'true')
	        {
	          $("#UserModal").modal('hide');
	          getUser();
	          $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
	          $('#addUserForm')[0].reset();

	        }else{

	          $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

	        }

	      }


	      })

	  		return false;

	  });

	  });

	  function getUser()
	  {
	    $("#result").html("Loading...");
	    $.get('{{ route("user.list") }}',function(data){

	     $("#result").html(data);

	     $('#userTable').DataTable({
	     			responsive: true,
	     	          language: {
	     	            searchPlaceholder: 'Search...',
	     	            sSearch: '',
	     	            
	     	          }
	     });

	     deleteConfirm();

	   });
	  }

	  function edit(id)
	  {
	   $("#editUserModal").modal();

	   $.get('{{ route("edit.user") }}',{id:id},function(data){
	     $("#editResult").html(data);
	     $('#userTable').DataTable();
	   });

	 }

	 function deleteConfirm()
	 {
	  $('.deleteFrm').on('submit', function () {

	    var title = $(this).find('.btn-delete').attr('data-title');

	    var poll = confirm("Confirm Delete ?");

	    if(poll)
	    {
	      return true;
	    }
	    else
	    {
	      return false;
	    }

	  })
	}

</script>

@endpush