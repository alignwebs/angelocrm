@extends('layouts.app')
@section('title', $title)
@section('content')
<style>
	.table {font-size: 12px;}
	.btn-xs {
	  padding: .25rem .4rem;
	  font-size: .875rem;
	  line-height: .5;
	  border-radius: .2rem;
	}
</style>

    <div class="row">
    	
        <div class="col-sm-6 col-xl-2 mg-t-20 mg-sm-t-0">
          <div class="bg-info rounded overflow-hidden">
            <div class="pd-x-20 pd-t-20 d-flex align-items-center">
              <i class="ion ion-ios-person tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Pending Leads</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $total['pending'] }}</p>
                
              </div>
            </div>
            <div id="ch3" class="ht-25 tr-y-1"></div>
          </div>
        </div>
        <div class="col-sm-6 col-xl-2 mg-t-20 mg-sm-t-0">
          <div class="bg-purple rounded overflow-hidden">
            <div class="pd-x-20 pd-t-20 d-flex align-items-center">
              <i class="ion ion-ios-person tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Moved Leads</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $total['moved'] }}</p>
                
              </div>
            </div>
            <div id="ch3" class="ht-25 tr-y-1"></div>
          </div>
        </div>
        <div class="col-sm-6 col-xl-2 mg-t-20 mg-sm-t-0">
          <div class="bg-teal rounded overflow-hidden">
            <div class="pd-x-20 pd-t-20 d-flex align-items-center">
              <i class="ion ion-ios-person tx-60 lh-0 tx-white op-7"></i>
              <div class="mg-l-20">
                <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Leads</p>
                <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $total['total'] }}</p>
                
              </div>
            </div>
            <div id="ch3" class="ht-25 tr-y-1"></div>
          </div>
        </div>
</div>
<br>
<div class="row">
  	<div class="col-lg-12">
  		 @include('flash::message')
  		<div id="success"></div>

  		<div class="card shadow-base bd-0">
  		    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
  		      	<h6 class="card-title tx-uppercase tx-12 mg-b-0">Manual Leads List</h6>
  		    </div><!-- card-header -->
  		    <div class="card-body justify-content-between align-items-center">
  		    	<div>
  		    		<button class="btn btn-lg btn-primary pull-right" type="button" id="uploadLeads"><i class="ion ion-ios-upload"></i> Upload</button>
  		    	</div>
  		    	<br>
  		    		                		<div id="result" style="margin-top: 37px">
  		    									
  		    								    <table class="table table-bordered table-condensed table-hover" id="leadstable" style="width: 100%;">
  		    								        <thead>
  		    							
  		    								            <tr>
  		    								            	<th></th>
  		    								                <th>#</th>
  		    								                <th>{{ tableHeader('name') }}</th>
  		    								                <th>{{ tableHeader('email') }}</th>
  		    								                <th>{{ tableHeader('flux_visitor') }}</th>
  		    								                <th>{{ tableHeader('flux_hid') }}</th>
  		    								                <th>{{ tableHeader('offer_name') }}</th>
  		    								                <th>s1</th>
  		    								                <th>s2</th>
  		    								                <th>s3</th>
  		    								                <th>s4</th>
  		    								                <th>s5</th>
  		    								                <th>payout</th>
  		    								                <th>status</th>
  		    								                <th>log</th>
  		    								                <th></th>
  		    								            </tr>
  		    								        </thead>
  		    								        
  		    								        <tbody>
  		    								       
  		    								        </tbody>

  		    								        <tfoot>
  		    								        		<th></th>
  		    								                <th>#</th>
  		    								                <th>{{ tableHeader('name') }}</th>
  		    								                <th>{{ tableHeader('email') }}</th>
  		    								                <th>{{ tableHeader('flux_visitor') }}</th>
  		    								                <th>{{ tableHeader('flux_hid') }}</th>
  		    								                <th>{{ tableHeader('offer_name') }}</th>
  		    								                <th>s1</th>
  		    								                <th>s2</th>
  		    								                <th>s3</th>
  		    								                <th>s4</th>
  		    								                <th>s5</th>
  		    								                <th>payout</th>
  		    								                <th>status</th>
  		    								                <th>log</th>
  		    								                <th></th>
  		    								        </tfoot>
  		    								    </table>
  		    								</div>
  		    <div class="table-rows-action-btns">
  		    	<div class="btn-group" role="group" aria-label="...">
  		    		  <button type="button" onclick="editRow()" class="btn btn-default btn-edit" disabled="disabled"><i class="glyphicon glyphicon-pencil"></i> Edit</button>
  		    		  <button type="button" onclick="deleteRows()" class="btn btn-default btn-delete"  disabled="disabled"><i class="glyphicon glyphicon-trash"></i> Delete</button>
  		    		</div>

  		    	<div class="stats"><span class="selected-rows-all"></span>&nbsp;&nbsp;<span><a href="javascript:void(0)" onclick="clearAll()">clear all</a></span></div>
  		    </div>								
  		    </div><!-- card-body -->

  		</div>
  		
  			 
  	</div>



</div>

<div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Deleting Records...</h4>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default btn-close refresh" style="display: none;" data-dismiss="modal">Close</button>
		<div class="progress" style="margin-bottom: 0"> <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:100%"><span class="sr-only"></span></div> </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Lead</h4>
      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="upload-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload CSV to Add Leads</h4>
      	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="container">
       	<form method="POST" id="filterfrm" enctype="multipart/form-data">
       		{{ csrf_field() }}
       		<div class="row">
       			<div class="col-sm-8">
       				<input type="file" class="custom-file-input" required name="csv_file" id="customFile"  accept=".xlsx, .xls, .csv">
       				<label class="custom-file-label custom-file-label-primary" for="customFile2">Choose file</label>
       			</div>
       			<div class="col-sm-4"><button class="btn btn-lg btn-primary" type="submit">Upload</button></div>
       		</div>
       		 
       	</form>
       </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection




@push('scripts')

<script>
	var selected = [];

	var table;
	$(document).ready(function() {
			getAll();

			$("#uploadLeads").click(function(){

				$("#upload-modal").modal();
			});
	});


	function getAll()
	{
				table = $('#leadstable').DataTable({
					responsive: true,
					searchDelay: 1000,
					columnDefs: [ {
				            orderable: false,
				            className: 'select-checkbox',
				            searchable: false,
				            targets:   0,
				            width: "5px",
			        },

			        {
			            targets: -1,
			            data: null,
			            searchable: false,
			            orderable: false,
			            defaultContent: '<button class="btn btn-xs btn-primary editRow"><i class="fa fa-edit"></i></button>'
			        }

			        ],
			        stateSave: true,
			        select: {
			            style:    'multi',
			            selector: 'td:first-child'
			        },
			        order: [[ 1, 'desc' ]],
					dom: 'Bfrtip',
			        "rowCallback": function( row, data ) {
			            if ( $.inArray(data.id, selected) !== -1 ) {
			                $(row).addClass('selected');
			            }
			        },
			        buttons: [
			        	'pageLength',
				        'selectAll',
				        'selectNone',
				        'colvis'
				    ],

				    language: {
				        buttons: {
				            selectAll: "Select all items",
				            selectNone: "Select none",
				            colvis: "Show Columns",
				        },
				        searchPlaceholder: 'Search...',
			            sSearch: '',
				    },
			        processing: true,
			        serverSide: true,
			        ajax: '{!! route('manual-leads-allData') !!}',

			        columns: [
			         	{ data: null, name: null },
			         	{ data: 'id', name: 'id' },
			            { data: 'name', name: 'name' },
			            { data: 'email', name: 'email' },
			            { data: 'flux_visitor', name: 'flux_visitor' },
			            { data: 'flux_hid', name: 'flux_hid' },
			            { data: 'offer_name', name: 'offer_name' },
			            { data: 's1', name: 's1' },
			            { data: 's2', name: 's2' },
			            { data: 's3', name: 's3' },
			            { data: 's4', name: 's4' },
			            { data: 's5', name: 's5' },
			            { data: 'payout', name: 'payout' },
			            { data: 'status', name: 'status' },
			            { data: 'log', name: 'log' },
			            { data: null, name: null },

		        	]
			    });

				/* Hide Columns By Default */
			    table.columns( [7,8,9,10,11] ).visible( false, false );

				// On Table Row Select

			    table.on( 'select', function ( e, dt, type, indexes ) {
			    	
				    if ( type === 'row' ) {

				    	if(indexes.length > 1)
				    	{
				    		console.log('multiple')
				    		$.each(indexes, function (i) {
					    		var data = table.rows( i ).data().pluck( 'id' );
					 			var id = data[0];
						        var index = $.inArray(id, selected);
						 
						        if ( index === -1 ) {
						            selected.push( id );
						        } 
					    	})
				    	}
				    	else
				    	{
				    		console.log('single')
				    			var data = table.rows( indexes ).data().pluck( 'id' );
					 			var id = data[0];
						        var index = $.inArray(id, selected);
						 	
						        if ( index === -1 ) {
						            selected.push( id );
						        } else {
						            selected.splice( index, 1 );
						        }
				    	}
				    	
				    	
				        	
				    }

				    reinit();
				} );

			    // On Table Row Deselect

				table.on( 'deselect', function ( e, dt, type, indexes ) {
			    
				    if ( type === 'row' ) {
				    	
				    	$.each(indexes, function (i) {
				    		var data = table.rows( i ).data().pluck( 'id' );
				 			var id = data[0];
					        var index = $.inArray(id, selected);
					 		selected.splice( index, 1 );	
				    	})
				        	
				    }

				     reinit();
				} );

				// On Table Initiation Complete
				table.on( 'draw', function () {
			        reinit();
			    })
			
		}

function reinit()
	{
		$('.select-checkbox').text(null);

		if(selected.length > 0)
		{
			$('.btn-delete').removeAttr('disabled');

			if(selected.length > 1)
				$('.btn-edit').attr('disabled','disabled');
			else
				$('.btn-edit').removeAttr('disabled');
		}
		else
		{
			$('.btn-edit, .btn-delete').attr('disabled','disabled');
		}


		$('.selected-rows-all').html("Total Rows Selected: "+selected.length)

	}

	function clearAll()
	{
		selected = [];
		table.rows('.selected').deselect();
		reinit();
	}


	function deleteRows()
	{
		if(selected.length > 0)
		{
			var poll = confirm("Are you sure to delete "+selected.length+" selected rows ?");

			if(poll)
			{
				if($('#users-table-all').is(':visible'))
				{
					$('.modal-footer .btn-close').attr('onclick','window.location.reload()');
				}
				else
				{
					$('.modal-footer .btn-close').removeAttr('onclick');
				}

				$('#delete-modal .modal-body').html("");
				$('#delete-modal').modal({
				  keyboard: false,
				  backdrop: 'static'
				},'show');

				$.ajax({
					url: "{{ route('delete-manual-leads') }}",
					method: "POST",
					data: {'ids[]':selected},
					xhrFields: {
						onprogress: function(e) {
							$('#delete-modal .modal-body').html(e.target.responseText)
							if (e.lengthComputable) {
								console.log(e.loaded / e.total * 100 + '%');
							}
						}
					},
					success: function(text) {
							
							//$('#sync-modal .modal-body').html(text+"<h1>done!</h1>")
							$('#delete-modal .progress').hide();
							$('#delete-modal .refresh').show();
							//alert("Sync Completed!")

							$('tr.selected').each(function () {
					
								$(this).find('td.select-checkbox').removeClass('select-checkbox');	
							})

							clearAll()

					}
				});

				
			}
		}
	}

	function editRow()
	{
		var id = selected[0];
		if(id > 0)
		{
			$('#edit-modal .modal-body').html("Loading...")

			$('#edit-modal .modal-body').load('{{ route('manual-leads-edit') }}?id='+id);

			$('#edit-modal').modal('show');
		}
	}

	$('table tbody').on( 'click', '.editRow', function () {
	        var data = table.row( $(this).parents('tr') ).data();
			//console.log(data)
			var id = data['id'];

			if(!id)
				id = data[1];

				if(id > 0)
				{
					$('#edit-modal .modal-body').html("Loading...")

					$('#edit-modal .modal-body').load('{{ route('manual-leads-edit') }}?id='+id);

					$('#edit-modal').modal('show');
				}

    	});
</script>

@endpush