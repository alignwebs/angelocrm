<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware'=>['auth','2fa']], function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    
    Route::get('/offer-name', 'OfferController@offerNameList')->name('offer-name');

    Route::get('/search/user/aweber', 'HomeController@getUserFromAweber')->name('aweber-search-user');

    Route::get('/leads', 'LeadsController@viewSearch')->name('leads');
    Route::get('/doSearch', 'LeadsController@doSearch')->name('do-search');
    Route::any('/allData', 'LeadsController@allData')->name('allData');
    Route::post('/leads/delete', 'LeadsController@delete')->name('delete-leads');
    Route::get('/leads/edit', 'LeadsController@edit')->name('edit-leads');
    Route::post('/leads/edit', 'LeadsController@update')->name('update-leads');
    
    Route::any('/manual-leads', 'ManualLeadsController@index')->name('manual-leads');
    Route::any('/manual-leads/sync', 'ManualLeadsController@sync')->name('manual-leads-sync');
    Route::any('/manual-leads/clean', 'ManualLeadsController@cleanRecords')->name('manual-leads-clean');
    Route::get('/manual-leads/all-data', 'ManualLeadsController@allData')->name('manual-leads-allData');
    Route::get('/manual-leads/edit', 'ManualLeadsController@edit')->name('manual-leads-edit');
    Route::post('/manual-leads/edit', 'ManualLeadsController@update')->name('update-manual-leads');
    Route::post('/manual-leads/delete', 'ManualLeadsController@delete')->name('delete-manual-leads');


    Route::get('/flux-offers/list', 'FluxOfferController@list')->name('flux-offers-list');
    Route::resource('flux-offers', 'FluxOfferController');

    Route::post('/otp/generate', 'OTPController@generate')->name('otp-generate');
    Route::post('/otp/verify', 'OTPController@verify')->name('otp-verify');

    Route::get('/offer-condition/editOfferCondition', 'OfferConditionController@editOfferCondition')->name('edit-offerCondition');
    Route::get('/offer-condition/list', 'OfferConditionController@list')->name('offer-condition-list');
    Route::resource('offer-condition', 'OfferConditionController');


    // NETWORK

    Route::get('/edit/network', 'NetworkController@edit')->name('edit.network');
    Route::post('/update/network', 'NetworkController@updateNetwork')->name('update.network');
    Route::get('/network/list', 'NetworkController@list')->name('network.list');
    Route::resource('network', 'NetworkController');
    // END

    // NETWORK PLATFORM

    Route::get('/edit/platform', 'NetworkPlatformController@edit')->name('edit.platform');
    Route::post('/update/platform', 'NetworkPlatformController@updatePlatform')->name('update.platform');
    Route::get('/platform/list', 'NetworkPlatformController@list')->name('platform.list');
    Route::resource('platform', 'NetworkPlatformController');
    // END

    // USER

    Route::get('/edit/user', 'UserController@edit')->name('edit.user');
    Route::post('/update/user', 'UserController@updateUser')->name('update.user');
    Route::get('/user/list', 'UserController@list')->name('user.list');
    Route::resource('user', 'UserController');
    // END


});

Route::get('/complete-registration', 'Auth\RegisterController@completeRegistration');

Route::post('/2fa', function () {
    return redirect('/');
})->name('2fa')->middleware('2fa');


Route::get('/re-authenticate', 'HomeController@reauthenticate')->name('re-authenticate');

Route::permanentRedirect('/home', '/dashboard', 301);
Route::permanentRedirect('/', '/login', 301);

Route::group(['prefix'=>'offers'], function () {

        Route::post('/edit', 'OfferController@update')->name('update-offers');
        Route::get('/edit', 'OfferController@edit')->name('offers-edit');
        Route::get('/list', 'OfferController@offerList')->name('offers-list');
        Route::any('/all-data', 'OfferController@allData')->name('offers-allData');
        Route::get('/networks/list', 'OfferController@getNetworkList');

        Route::get('/sync/account/cake', 'OfferController@syncCakeOffers')->name('sync-account-cake');
        Route::get('/sync/offers/cake', 'OfferController@syncCakeOfferStatus')->name('sync-offers-cake');

        Route::get('/sync/account/hasoffer', 'OfferController@syncHasOffers')->name('sync-account-hasoffer');
        Route::get('/sync/offers/hasoffer', 'OfferController@syncHasOfferStatus')->name('sync-offers-hasoffer');

        Route::get('/sync/account/everflow', 'OfferController@syncEverflow')->name('sync-account-everflow');

        Route::get('/status/{type}/{id}/', 'OfferController@offerStatus');

        Route::post('/status/array', 'OfferController@offerStatusArray')->name('offer-status-array'); // Added App\Http\Middleware\VerifyCsrfToken
});

