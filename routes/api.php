<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/flux-offer/pageid/{pageID}', 'OfferController@FluxPageIdOffer');

Route::get('/rotator/process', 'RotatorController@process')->name('rotator-process');
Route::get('/rotator/saveLastOnDone', 'RotatorController@saveLastOnDone')->name('rotator-saveLastOnDone');
