<?php

// https://crm.roitracker.io/api/offers/sync/offers/cake
// https://crm.roitracker.io/api/offers/sync/offers/hasoffer

$rotator_type = "fulfillment"; // [fulfillment,spillover,sequential,random]

require_once _DIR_ . './../admin/api/v2/toolbox.php';

$cacheExpirationInMinutes = 5;
$checkLastHours = '[[epv-auto-routing-lasthours]]';
$checkLastHours = empty($checkLastHours) ? 24 : $checkLastHours;

$cacheKey = '{node-id}-{trafficsource-id}';
$cache = Cache::getInstance( Cache::TYPE_FILE );
$nodeWeights = $cache->get( $cacheKey );

if( $nodeWeights === Cache::NOT_FOUND )
{
  $nodeWeights = [];
  $nodeConnections = DBTableCampaignFunnelConnections::getNodeExitConnections( '{node-id}' );
  
  if( !empty( $nodeConnections ) )
  {
    $nodeIds = [];
    foreach( $nodeConnections as $connection )
    {
      $targetNodeId = $connection->getTargetNodeId();
      $nodeIds[] = $targetNodeId;
      $nodeWeights[ $targetNodeId ] = -1;
      
      $fetchNodeConnection = @file_get_contents('http://roitracker.io/admin/api/v2/data/campaign/funnel/connection/find/byTargetNodeId/index.php?apiKey=4BD0-B2B2&idTargetNode='.$targetNodeId);
      $fetchNodeConnection = json_decode($fetchNodeConnection,true);
      
      
      $fetchNodes = file_get_contents("http://roitracker.io/admin/api/v2/data/campaign/funnel/node/find/byId/index.php?apiKey=4BD0-B2B2&idNode=".$targetNodeId);
      $fetchNodes = json_decode($fetchNodes,true);
      
      if(sizeof($fetchNodes) > 0)
      {
        $pageID = $fetchNodes['nodePageParams']['idPage'];
        if(!empty($pageID))
        {
          $fetchOffer = @file_get_contents('http://crm.roitracker.io/api/flux-offer/pageid/'.$pageID);
          $fetchOffer = json_decode($fetchOffer,true);
          $offers[] = array(
            "offer_id"=>$fetchOffer['offer_id'],
            "type"=>$fetchOffer['offer_type'],
            "name"=>utf8_encode($fetchOffer['offer_name']),
            "offer_status"=>strtolower($fetchOffer['offer_status']),
            "onDoneNumber" => $fetchNodeConnection[0]['connectionCodeParams']['onDoneNumber'],
            "PageID"=>$pageID,
            "targetNodeId"=>$targetNodeId
          );
        }
      }
    } 

    /* Add Fallback Node as Last Done Offer*/

    $offers[] = array(
      "offer_id"=>"",
      "type"=>"node",
      "name"=>"",
      "offer_status"=>"active",
      "onDoneNumber" => sizeof($offers) + 1,
      "PageID"=>"",
      "targetNodeId"=>$targetNodeId
    );
    
    /* Sort Offers By onDoneNumber */
    usort($offers, function($a, $b) {
      return $a['onDoneNumber'] - $b['onDoneNumber'];
    });

  }
}

$visitor_id = "{visitor-id}";
$node_id = "{node-id}";

$api_data = @file_get_contents('http://crm.roitracker.io/api/rotator/process?visitorid='.$visitor_id.'&node_id='.$node_id);
$api_data = json_decode($api_data,true);


switch($rotator_type)
{
  case "fulfillment":
  {
    if(sizeof($offers) > 0)
    {
      foreach ($offers as $offer) {
        if($offer['offer_id'] > 0)
        {
          if($offer['offer_status'] == 'inactive')
            continue;
          else
            return $offer['onDoneNumber'];
        }
        else
         return $offer['onDoneNumber'];
     }
   }
   break;
 }
 case "spillover":
 {
  $onDoneNumber = processOnDone($offers,$api_data['last_visitor_onDoneNumber']);
  saveLastOnDone($visitor_id,$node_id,$onDoneNumber,$rotator_type);

  return $onDoneNumber;
  break;
}
case "sequential":
{

  $onDoneNumber = processOnDone($offers,$api_data['last_node_onDoneNumber']);

  saveLastOnDone($visitor_id,$node_id,$onDoneNumber,$rotator_type);

  return $onDoneNumber;
  break;
}
case "random":
{  
  return processRandom($offers);
  break;
}

default:{

  return firstActiveOffer($offers);
  break;
}
}


/* Functions */

function saveLastOnDone($visitor_id,$node_id,$onDoneNumber,$rotator_type)
{
  @file_get_contents('http://crm.roitracker.io/api/rotator/saveLastOnDone?visitorid='.$visitor_id.'&node_id='.$node_id.'&onDoneNumber='.$onDoneNumber.'&rotator_type='.$rotator_type);
}


function processOnDone($offers, $visitor_last_onDoneNumber)
{

  $offer_size = sizeof($offers);
  $onDoneNumber = 1;
  $type = "start";

  if($visitor_last_onDoneNumber > 0){
    $keyFound = array_search($visitor_last_onDoneNumber, array_column($offers, 'onDoneNumber'));

    if($keyFound > -1)
    {
      $nextKey = $keyFound + 1;
      if($nextKey >= $offer_size){
        $type = "key length mismatch";
        $onDoneNumber = firstActiveOffer($offers);
      }
      else{
        if($offers[$nextKey]['offer_status'] == 'inactive'){
          $type = "inactive offer found";
          return processOnDone($offers,$offers[$nextKey]['onDoneNumber']);
        }
        else{
          $type = "next active offer found";
          $onDoneNumber = $offers[$nextKey]['onDoneNumber'];

        }
      }
    }
    else
    {
     $type = "key not found";
     $onDoneNumber = firstActiveOffer($offers);
   }
 }
 else
 {
   $type = "on done not found";
   $onDoneNumber = firstActiveOffer($offers);
 }

 return $onDoneNumber;
}

function firstActiveOffer($offers)
{
   $keyFound = array_keys(array_column($offers, 'offer_status'),'active');
   return $offers[$keyFound[0]]['onDoneNumber'];
}

function OffersInactive($offers)
{
	foreach ($offers as $offer) {
     if($offer['offer_id'] > 0)
     {
       if($offer['offer_status'] == 'inactive')
         continue;
       else
         return false;
     }
   }
  return true;
}

function processRandom($offers)
{
  $rand_key = array_rand($offers);

  $offer = $offers[$rand_key];

  if($offer['offer_id'] > 0)
  {
    if($offer['offer_status'] == 'inactive'){
      unset($offers[$rand_key]);
      return processRandom($offers);
    }
    else{
      return $offer['onDoneNumber'];
    }
  }
  else
    return (sizeof($offers) + 1);
}