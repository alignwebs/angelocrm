<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $appends = ['full_offer_name','network_name','network_platform_name'];

    public function network()
    {
        return $this->belongsTo('App\Network');
    }

    public function scopeNexus($query)
    {
        return $query->where('network_id', 'nexusoffers');
    }

    public function scopeActive($query)
    {
        return $query->where('offer_status', '!=', 'inactive');
    }

    public function getFullOfferNameAttribute()
    {
        return $this->offer_name." - ".$this->network_id;
    }

    public function getNetworkNameAttribute()
    {
        return $this->network->name;
    }

    public function getNetworkPlatformNameAttribute()
    {
        return $this->network->network_platform->platform_name;
    }

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
