<?php

if (! function_exists('ModelBtn')) {

    function ModelBtn($route, $id, $title = "")
    {
        $html = '<div class="btn-group btn-options" role="group">';

        $html.= Form::open(['route' => [$route.'.destroy', $id], 'method' => 'delete', 'class' => 'deleteFrm']);
        //$html.= '<a href="'.URL::route($route.'.edit',$id).'" class="btn btn-default btn-sm"><span class="fa fa-edit" aria-hidden="true"></span> Edit</a>';
        $html.= '<button type="submit" class="btn-delete btn btn-danger btn-sm" data-title="'.$title.'"><span class="fa fa-trash" aria-hidden="true"></span> </button>';
        $html.= Form::close();

        $html.= '</div>';
        return $html;
    }
}

if (! function_exists('ModelBtn2')) {

    function ModelBtn2($route, $id, $title = "")
    {
        $html = '<div class="btn-group btn-options" role="group">';

        $html.= Form::open(['route' => [$route.'.destroy', $id], 'method' => 'delete', 'class' => 'deleteFrm']);
        $html.= '<a href="javascript:void(0)" onclick="edit('.$id.')" class="btn btn-primary btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></a>&nbsp;&nbsp;';
        $html.= '<button type="submit" class="btn-delete btn btn-danger btn-sm" data-title="'.$title.'"><span class="fa fa-trash" aria-hidden="true"></span> </button>';
        $html.= Form::close();

        $html.= '</div>';
        return $html;
    }
}

if (!function_exists('csvToArray')) {
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

            $header = null;
            $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

            return $data;
    }
}

if (!function_exists('tableHeader')) {
    function tableHeader($value)
    {
        $fields = [

            'email' => 'Email Address',
            'flux_visitor' => 'Flux Visitor ID',
            'offer_name' => 'Offer Name',
            'payout' => 'Payout',
            'name' => 'Name',
            'flux_hid' => 'Flux Hit ID',
        ];

        if (isset($fields[$value])) {
            return $fields[$value];
        } else {
            return $value;
        }
    }
}

if (!function_exists('getUserFromAweber')) {
    function getUserFromAweber($email)
    {
        if ($email != "") {
            $response = file_get_contents('http://roitracker.io/api/getUser.php?email='.$email);
            $response = json_decode($response, true);

            if (isset($response['data'])) {
                return $response['data'];
            }
        }
    }
}

if (!function_exists('getAffiliateNetworks')) {
    function getAffiliateNetworks()
    {
        $api['cake'] = [
        ['name'=>'nexusoffers', 'api_key'=>'3lEy6gtjQlyAB5SypF9HA', 'affiliate_id'=>'167', 'domain'=>'admin.nexusoffers.com', 'timezone'=>'America/New_York'],
        ['name'=>'cashnetwork', 'api_key'=>'j0MkCzfSkLM8rF26UEDNyg', 'affiliate_id'=>'18039', 'domain'=>'admin.cashnetwork.com', 'timezone'=>'America/New_York'],
        ['name'=>'epcboss', 'api_key'=>'uSKfxdxChuzsJW1dtTWucw', 'affiliate_id'=>'131', 'domain'=>'track.epcboss.com', 'timezone'=>'America/Los_Angeles'],
        ['name'=>'bigepc', 'api_key'=>'UNBBF9wAzdPB3lytKJvmQ', 'affiliate_id'=>'56', 'domain'=>'app.bigepc.com', 'timezone'=>'US/Eastern'],
        ['name'=>'oasisads', 'api_key'=>'BnOdVhcptvtt81DYtU7jQ', 'affiliate_id'=>'205715', 'domain'=>'oastrk.com', 'timezone'=>'America/New_York'],
        ['name'=>'frstmedia', 'api_key'=>'E45VYkjFGBi75p4YZCJbA', 'affiliate_id'=>'2607', 'domain'=>'partners.frstmedia.com', 'timezone'=>'America/New_York'],
        ];

        $api['hasoffer'] = [
        ['name'=>'oppur2nity', 'api_key'=>'5a170cc0195c4ea4b2c07d557738161e532413d77dcc3d9c41f176be6c3133f4', 'affiliate_id'=>'oppur2nity', 'domain'=>'oppur2nity.api.hasoffers.com'],
        ['name'=>'apexbrands', 'api_key'=>'7387db069b04fda38556f754a5ee21fbcc86b22740902c50319616eddea83153', 'affiliate_id'=>'apexbrands', 'domain'=>'apexbrands.api.hasoffers.com'],
        ];

        return $api;
    }
}

if (!function_exists('offerStatus')) {
    function offerStatus($status)
    {
        $status = strtolower($status);

        switch ($status) {
            case "active":
                $class = "label label-success";
                break;

            default:
                $class = "label label-default";
                break;
        }

        return '<span class="'.$class.'">'.$status.'</span>';
    }
}

if(!function_exists('format_text')){
        function format_text($text)
        {
            $text = str_replace("_", " ", $text);
            $text = ucwords($text);
            return $text;
        }
}

if(!function_exists('timezone')){


    function timezone()
    {
        return $timezones = array(
            
                'Pacific/Midway'       => "(GMT-11:00) Midway Island",
                'US/Samoa'             => "(GMT-11:00) Samoa",
                'US/Hawaii'            => "(GMT-10:00) Hawaii",
                'US/Alaska'            => "(GMT-09:00) Alaska",
                'US/Pacific'           => "(GMT-08:00) Pacific Time (US &amp; Canada)",
                'America/Tijuana'      => "(GMT-08:00) Tijuana",
                'US/Arizona'           => "(GMT-07:00) Arizona",
                'US/Mountain'          => "(GMT-07:00) Mountain Time (US &amp; Canada)",
                'America/Chihuahua'    => "(GMT-07:00) Chihuahua",
                'America/Mazatlan'     => "(GMT-07:00) Mazatlan",
                'America/Mexico_City'  => "(GMT-06:00) Mexico City",
                'America/Monterrey'    => "(GMT-06:00) Monterrey",
                'Canada/Saskatchewan'  => "(GMT-06:00) Saskatchewan",
                'US/Central'           => "(GMT-06:00) Central Time (US &amp; Canada)",
                'US/Eastern'           => "(GMT-05:00) Eastern Time (US &amp; Canada)",
                'US/East-Indiana'      => "(GMT-05:00) Indiana (East)",
                'America/Bogota'       => "(GMT-05:00) Bogota",
                'America/Lima'         => "(GMT-05:00) Lima",
                'America/Caracas'      => "(GMT-04:30) Caracas",
                'Canada/Atlantic'      => "(GMT-04:00) Atlantic Time (Canada)",
                'America/La_Paz'       => "(GMT-04:00) La Paz",
                'America/Santiago'     => "(GMT-04:00) Santiago",
                'Canada/Newfoundland'  => "(GMT-03:30) Newfoundland",
                'America/Buenos_Aires' => "(GMT-03:00) Buenos Aires",
                'Greenland'            => "(GMT-03:00) Greenland",
                'Atlantic/Stanley'     => "(GMT-02:00) Stanley",
                'Atlantic/Azores'      => "(GMT-01:00) Azores",
                'Atlantic/Cape_Verde'  => "(GMT-01:00) Cape Verde Is.",
                'Africa/Casablanca'    => "(GMT) Casablanca",
                'Europe/Dublin'        => "(GMT) Dublin",
                'Europe/Lisbon'        => "(GMT) Lisbon",
                'Europe/London'        => "(GMT) London",
                'Africa/Monrovia'      => "(GMT) Monrovia",
                'Europe/Amsterdam'     => "(GMT+01:00) Amsterdam",
                'Europe/Belgrade'      => "(GMT+01:00) Belgrade",
                'Europe/Berlin'        => "(GMT+01:00) Berlin",
                'Europe/Bratislava'    => "(GMT+01:00) Bratislava",
                'Europe/Brussels'      => "(GMT+01:00) Brussels",
                'Europe/Budapest'      => "(GMT+01:00) Budapest",
                'Europe/Copenhagen'    => "(GMT+01:00) Copenhagen",
                'Europe/Ljubljana'     => "(GMT+01:00) Ljubljana",
                'Europe/Madrid'        => "(GMT+01:00) Madrid",
                'Europe/Paris'         => "(GMT+01:00) Paris",
                'Europe/Prague'        => "(GMT+01:00) Prague",
                'Europe/Rome'          => "(GMT+01:00) Rome",
                'Europe/Sarajevo'      => "(GMT+01:00) Sarajevo",
                'Europe/Skopje'        => "(GMT+01:00) Skopje",
                'Europe/Stockholm'     => "(GMT+01:00) Stockholm",
                'Europe/Vienna'        => "(GMT+01:00) Vienna",
                'Europe/Warsaw'        => "(GMT+01:00) Warsaw",
                'Europe/Zagreb'        => "(GMT+01:00) Zagreb",
                'Europe/Athens'        => "(GMT+02:00) Athens",
                'Europe/Bucharest'     => "(GMT+02:00) Bucharest",
                'Africa/Cairo'         => "(GMT+02:00) Cairo",
                'Africa/Harare'        => "(GMT+02:00) Harare",
                'Europe/Helsinki'      => "(GMT+02:00) Helsinki",
                'Europe/Istanbul'      => "(GMT+02:00) Istanbul",
                'Asia/Jerusalem'       => "(GMT+02:00) Jerusalem",
                'Europe/Kiev'          => "(GMT+02:00) Kyiv",
                'Europe/Minsk'         => "(GMT+02:00) Minsk",
                'Europe/Riga'          => "(GMT+02:00) Riga",
                'Europe/Sofia'         => "(GMT+02:00) Sofia",
                'Europe/Tallinn'       => "(GMT+02:00) Tallinn",
                'Europe/Vilnius'       => "(GMT+02:00) Vilnius",
                'Asia/Baghdad'         => "(GMT+03:00) Baghdad",
                'Asia/Kuwait'          => "(GMT+03:00) Kuwait",
                'Africa/Nairobi'       => "(GMT+03:00) Nairobi",
                'Asia/Riyadh'          => "(GMT+03:00) Riyadh",
                'Europe/Moscow'        => "(GMT+03:00) Moscow",
                'Asia/Tehran'          => "(GMT+03:30) Tehran",
                'Asia/Baku'            => "(GMT+04:00) Baku",
                'Europe/Volgograd'     => "(GMT+04:00) Volgograd",
                'Asia/Muscat'          => "(GMT+04:00) Muscat",
                'Asia/Tbilisi'         => "(GMT+04:00) Tbilisi",
                'Asia/Yerevan'         => "(GMT+04:00) Yerevan",
                'Asia/Kabul'           => "(GMT+04:30) Kabul",
                'Asia/Karachi'         => "(GMT+05:00) Karachi",
                'Asia/Tashkent'        => "(GMT+05:00) Tashkent",
                'Asia/Kolkata'         => "(GMT+05:30) Kolkata",
                'Asia/Kathmandu'       => "(GMT+05:45) Kathmandu",
                'Asia/Yekaterinburg'   => "(GMT+06:00) Ekaterinburg",
                'Asia/Almaty'          => "(GMT+06:00) Almaty",
                'Asia/Dhaka'           => "(GMT+06:00) Dhaka",
                'Asia/Novosibirsk'     => "(GMT+07:00) Novosibirsk",
                'Asia/Bangkok'         => "(GMT+07:00) Bangkok",
                'Asia/Jakarta'         => "(GMT+07:00) Jakarta",
                'Asia/Krasnoyarsk'     => "(GMT+08:00) Krasnoyarsk",
                'Asia/Chongqing'       => "(GMT+08:00) Chongqing",
                'Asia/Hong_Kong'       => "(GMT+08:00) Hong Kong",
                'Asia/Kuala_Lumpur'    => "(GMT+08:00) Kuala Lumpur",
                'Australia/Perth'      => "(GMT+08:00) Perth",
                'Asia/Singapore'       => "(GMT+08:00) Singapore",
                'Asia/Taipei'          => "(GMT+08:00) Taipei",
                'Asia/Ulaanbaatar'     => "(GMT+08:00) Ulaan Bataar",
                'Asia/Urumqi'          => "(GMT+08:00) Urumqi",
                'Asia/Irkutsk'         => "(GMT+09:00) Irkutsk",
                'Asia/Seoul'           => "(GMT+09:00) Seoul",
                'Asia/Tokyo'           => "(GMT+09:00) Tokyo",
                'Australia/Adelaide'   => "(GMT+09:30) Adelaide",
                'Australia/Darwin'     => "(GMT+09:30) Darwin",
                'Asia/Yakutsk'         => "(GMT+10:00) Yakutsk",
                'Australia/Brisbane'   => "(GMT+10:00) Brisbane",
                'Australia/Canberra'   => "(GMT+10:00) Canberra",
                'Pacific/Guam'         => "(GMT+10:00) Guam",
                'Australia/Hobart'     => "(GMT+10:00) Hobart",
                'Australia/Melbourne'  => "(GMT+10:00) Melbourne",
                'Pacific/Port_Moresby' => "(GMT+10:00) Port Moresby",
                'Australia/Sydney'     => "(GMT+10:00) Sydney",
                'Asia/Vladivostok'     => "(GMT+11:00) Vladivostok",
                'Asia/Magadan'         => "(GMT+12:00) Magadan",
                'Pacific/Auckland'     => "(GMT+12:00) Auckland",
                'Pacific/Fiji'         => "(GMT+12:00) Fiji",
            );
    }
}
    if(!function_exists('getRoles'))
    {
        function getRoles()
        {
            $roles = array(

                'admin'    => 'Admin',
                'sub_admin' => 'Sub Admin'
            );
            return $roles;
        }
    }
