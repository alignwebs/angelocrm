<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NetworkPlatform extends Model
{
    use SoftDeletes;

    public function networks()
    {
    	return $this->hasMany('App\Network');
    }
}
