<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\ManualLead;
use App\Lead;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $total['pendingLeadsTotal'] = Redis::get('stats:pending-leads') + Redis::get('stats:pending-manual-leads');
        $total['pendingLeadsToday'] = Redis::get('stats:pending-leads-today') + Redis::get('stats:pending-manual-leads-today');

        $total['buyersLeadsTotal'] = Redis::get('stats:buyers-leads') + Redis::get('stats:buyers-manual-leads');
        $total['buyersLeadsToday'] = Redis::get('stats:buyers-leads-today') + Redis::get('stats:buyers-manual-leads-today');

        $total['LeadsTotal'] = Redis::get('stats:total-leads') + Redis::get('stats:total-manual-leads');

        return view('home', compact('total'));
    }

    public function reauthenticate(Request $request)
    {
        // get the logged in user
        $user = \Auth::user();

        // initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // generate a new secret key for the user
        $user->google2fa_secret = $google2fa->generateSecretKey();

        // save the user
        $user->save();

        // generate the QR image
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $user->google2fa_secret
        );

        // Pass the QR barcode image to our view.
        return view('google2fa.register', ['QR_Image' => $QR_Image, 
                                            'secret' => $user->google2fa_secret,
                                            'reauthenticating' => true
                                        ]);
    }


    public function getUserFromAweber(Request $request)
    {
        return getUserFromAweber($request->input('email'));
    }
}
