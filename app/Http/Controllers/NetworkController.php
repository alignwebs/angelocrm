<?php

namespace App\Http\Controllers;

use App\Network;
use App\NetworkPlatform;
use Validator;
use Illuminate\Http\Request;

class NetworkController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $title = "Manage Networks";
        $network_platform = NetworkPlatform::pluck('platform_name','id');
        
        return view('network.index', compact('network_platform','title'));
    }


    public function list()
    {
        $rows = Network::all();
        
        return view('network.ajax.list', compact('rows'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
                
                'network_platform_id' => 'required',
                'name' => 'required',
                'api_key' => 'required',
                'domain' => 'required',
                'active'   => 'required'
              
        ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        

        $Network = new Network;
        
        $Network->network_platform_id = $request->input('network_platform_id');
        $Network->name = $request->input('name');
        $Network->api_key = $request->input('api_key');
        $Network->affiliate_id = $request->input('affiliate_id');
        $Network->domain = $request->input('domain');
        $Network->active = $request->input('active');
        $Network->timezone = $request->input('timezone');
        $Network->save(); 
        

        return response()->json(['success'=>'true','message'=>'Network has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Network  $Network
     * @return \Illuminate\Http\Response
     */
    public function show(Network $Network)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Network  $Network
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Network $Network)
    {
        $id = $request->id;

        $network = Network::where('id',$id)->first();
        
        $network_platform = NetworkPlatform::pluck('platform_name','id');

        return view('network.ajax.edit',compact('network','network_platform'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Network  $Network
     * @return \Illuminate\Http\Response
     */
    public function updateNetwork(Request $request, Network $Network)
    {
        
        $validator = Validator::make($request->all(), [
                
                'network_platform_id' => 'required',
                'name' => 'required',
                'api_key' => 'required',
                'domain' => 'required',
                'active'   => 'required'
              
        ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        

        $Network = Network::find($request->id);
        
        $Network->network_platform_id = $request->input('network_platform_id');
        $Network->name = $request->input('name');
        $Network->api_key = $request->input('api_key');
        $Network->affiliate_id = $request->input('affiliate_id');
        $Network->domain = $request->input('domain');
        $Network->active = $request->input('active');
        $Network->timezone = $request->input('timezone');
        $Network->save(); 
        

        return response()->json(['success'=>'true','message'=>'Network has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Network  $Network
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Network = Network::where('id',$id);
        $Network->delete();
        flash('Network Deleted Successfully')->success();
        return redirect()->back();
    }
}
