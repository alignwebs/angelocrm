<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitor;
use Validator;

class RotatorController extends Controller
{
    public function process(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'visitorid' => 'required','node_id' => 'required',
         ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $visitor_id     = $request->input('visitorid');
        $node_id        = $request->input('node_id');

        $data['node_visits_count'] = Visitor::where('node_id', $node_id)->count();

        $data['visitor_node_count'] = Visitor::where('visitor_id', $visitor_id)->where('node_id', $node_id)->count();

        $data['last_visitor_onDoneNumber'] = (int) Visitor::select('onDoneNumber')->where('node_id', $node_id)->where('visitor_id', $visitor_id)->latest('timestamp')->pluck('onDoneNumber')->first();

        $data['last_node_onDoneNumber'] = (int) Visitor::select('onDoneNumber')->where('node_id', $node_id)->latest('timestamp')->pluck('onDoneNumber')->first();

        return $data;
    }

    public function saveLastOnDone(Request $request)
    {
        $visitor_id     = $request->input('visitorid');
        $node_id        = $request->input('node_id');
        $rotator_type   = $request->input('rotator_type');
        $onDoneNumber   = $request->input('onDoneNumber');

        Visitor::insert(['visitor_id'=>$visitor_id,'node_id'=>$node_id,'onDoneNumber'=>$onDoneNumber,'rotator_type'=>$rotator_type,'timestamp'=>time()]);
    }
}
