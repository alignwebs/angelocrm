<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Lead;
use App\Tag;
use Schema;

class LeadsController extends Controller
{
    public function index()
    {
         return view('index');
    }

    public function allData()
    {
        return Datatables::of(Lead::query())->make(true);
    }

    public function viewSearch()
    {
        $Lead = new Lead();
        $coulmns = $Lead->getTableColumns();

        $filters = ['contains', 'is', 'is not'];
        $title = "Leads";
        return view('search', compact('coulmns', 'filters','title'));
    }

    public function doSearch(Request $request)
    {
        $input = $request->all();
        //dd($input);
        $columns = $request->column;
        $filters = $request->filter;
        $values = $request->value;

        $coulumns_count = sizeof($columns);

        $Lead = new Lead();
        
        
        for ($i=0; $i<$coulumns_count; $i++) {
            if ($filters[$i] == 'is') {
                $condition = '=';
                $value = $values[$i];
            }
            if ($filters[$i] == 'is not') {
                $condition = '!=';
                $value = $values[$i];
            }
            if ($filters[$i] == 'contains') {
                $condition = 'like';
                $value = '%'.$values[$i].'%';
            }
            
            $Lead = $Lead->where($columns[$i], $condition, $value);
        }


        return $Lead->get();
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');

        $row = Lead::findOrFail($id);

        $aweber_data = getUserFromAweber($row['email']);


        //dd($aweber_data);
        $row->tags = json_encode($aweber_data['tags']);

        $row->save();

        $row = $row->toArray();

        $aweber_tags = [];

        if (isset($aweber_data['tags'])) {
            foreach ($aweber_data['tags'] as $tag) {
                Tag::firstOrCreate(['name'=>$tag]);
                $aweber_tags[$tag] = $tag;
            }
        }

        $tags = Tag::all()->pluck('name', 'name');
        
         $form_action =  route('update-leads');

        return view('ajax.editlead', compact('row', 'aweber_data', 'tags', 'aweber_tags', 'form_action'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');

        $fields = $request->all();

        $tags_remove = [];

        if ($request->input('aweber_tags')) {
            $aweber_tags = $request->input('aweber_tags');
        } else {
            $aweber_tags = [];
        }

        if (sizeof($aweber_tags) > 0 and sizeof($request->input('tags')) > 0) {
            $tags_remove = array_diff($aweber_tags, $request->input('tags'));
        } else {
            $tags_remove = $aweber_tags;
        }
        
        $fields['tags'] = json_encode($request->input('tags'));

        $query_data = [
            'tags_add'    => $request->input('tags'),
            'tags_remove' => array_values($tags_remove),
            'email'       => $request->input('email'),
        ];

        echo file_get_contents('http://roitracker.io/api/updateUser.php?'.http_build_query($query_data));
        

        Arr::forget($fields, 'aweber_tags');
        $Lead = Lead::find($id);
        $Lead->update($fields);

        return "Record updated successfully!";
    }

    public function delete(Request $request)
    {
        $ids = $request->input('ids');

        foreach ($ids as $id) {
            if (Lead::find($id)->delete()) {
                echo $id.": Deleted Successfully! <br>";
            } else {
                echo $id.": Cannot Delete! <br>";
            }
       
            flush();
            ob_flush();
            sleep(1);
        }
    }
}
