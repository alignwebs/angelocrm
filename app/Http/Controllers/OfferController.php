<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webklex\IMAP\Client;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Offer;
use App\NetworkPlatform;

class OfferController extends Controller
{
    public $fields_persists;

    public function __construct()
    {
         $this->fields_persists = ['offer_url','offer_redirect_domain','flux_page_id'];
    }

    /* Cake Functions */

    public function syncCakeOffers()
    {
        set_time_limit(0);
        ignore_user_abort(1);

        //@file_get_contents('http://roitracker.io/api/del_test/log.php?log=syncing-cakeoffers');
        $NetworkPlatform = NetworkPlatform::with('networks')->where('platform_name','cake')->first();
        $cake_offers = $NetworkPlatform->networks;

        foreach ($cake_offers as $c_offer) {
                $rows = [];

                $api_key = $c_offer['api_key'];
                $affiliate_id = $c_offer['affiliate_id'];
                $offer_type = $c_offer['name'];

                $offer_rows = Offer::where('network_id', $c_offer->id)->get();


            try {
                $url = "https://".$c_offer['domain']."/affiliates/api/Offers/Feed?start_at_row=1&row_limit=300&api_key=".$api_key."&affiliate_id=".$affiliate_id;

                $fetch = file_get_contents($url);

                $fetch = json_decode($fetch, true);

                foreach ($fetch['data'] as $offer) {
                    $rows[] = array(
                        "offer_id" => $offer['offer_id'],
                        "offer_contract_id" => $offer['offer_contract_id'],
                        "campaign_id" => $offer['campaign_id'],
                        "offer_name" => utf8_encode($offer['offer_name']),
                        "price" => $offer['price'],
                        "network_id" => $c_offer->id,
                        "network_platform_id" => $NetworkPlatform->id,
                        "offer_status" => $offer['offer_status']['offer_status_name']
                    );
                }

                Offer::where('network_id', $offer_type)->delete();
                Offer::insert($rows);
            } catch (\Exception $e) {
                continue;
            }
        }

        @file_get_contents('http://roitracker.io/api/del_test/log.php?log=syncing-cakeoffers-finsihed');


        return response()->json([
                'success' => 'true',
                'message' => 'Offers synced successfully!',
            ]);
    }


    public function syncCakeOfferStatus()
    {
        set_time_limit(0);
        ignore_user_abort(1);

        $NetworkPlatform = NetworkPlatform::with('networks')->where('platform_name','cake')->first();
        $cake_network = $NetworkPlatform->networks;
        
        $offerStatus = "";

        foreach ($cake_network as $network) {
            $api_key = $network['api_key'];
            $affiliate_id = $network['affiliate_id'];
            $offer_type = $network['name'];

            date_default_timezone_set($network['timezone']);
            $start_date = date("Y-m-d 00:00:01");
            $end_date   = date("Y-m-d 23:59:59");
            //echo $start_date = date("Y-m-d H:i:s")."<br>";

            $offers = Offer::where('network_id', $network['name'])->get();

            foreach ($offers as $offer) {
                if ($offer->offer_status == "Apply To Run" or $offer->offer_status == "Pending") {
                    continue;
                }

                $oid = $offer->offer_id;

                $url = "https://".$network['domain']."/affiliates/api/Reports/Clicks?offer_id=".$oid."&start_date=".urlencode($start_date)."&end_date=".urlencode($end_date)."&start_at_row=1&row_limit=9999&sort_field=click_date&sort_descending=true&fields=click_date&fields=offer&fields=redirect_from_offer&api_key=".$api_key."&affiliate_id=".$affiliate_id;

                $fetch = file_get_contents($url);
                $fetch = json_decode($fetch, true);

                if (sizeof($fetch['data']) > 0) {
                    $offer_click = $fetch['data'][0]; // Fetch Lastest Click

                    if ($offer_click['offer']['offer_id'] != $oid and $offer_click['offer']['offer_id'] != 25) { // Foreign Check as 25 status
                        Offer::where('offer_id', $oid)->where('network_id', $offer->type)->update(['offer_status'=>'inactive']);
                        $offerStatus['inactive'][] = $oid;
                    } else {
                        Offer::where('offer_id', $oid)->where('network_id', $offer->type)->update(['offer_status'=>'Active']);
                        $offerStatus['active'][] = $oid;
                    }
                }
            }
        }

        return response()->json([
                'success' => 'true',
                'status' => 'Offers synced successfully!',
            ]);
    }

    /* HasOffers Functions */

    public function syncHasOffers()
    {
        set_time_limit(0);
        ignore_user_abort(1);

        $NetworkPlatform = NetworkPlatform::with('networks')->where('platform_name','hasoffer')->first();
        $networks = $NetworkPlatform->networks;

        foreach ($networks as $network) {
                $rows = [];

                $api_key = $network['api_key'];
                $affiliate_id = $network['affiliate_id'];
                $network_id = $network['id'];
                $network_platform_id = $network['network_platform_id'];
                $offer_type = $network['name'];

                Offer::where('network_id', $network_id)->delete();

                $url = "https://".$network['domain']."/Apiv3/json?api_key=".$api_key."&Target=Affiliate_Offer&Method=findMyApprovedOffers";

                $fetch = file_get_contents($url);
                $fetch = json_decode($fetch, true);

            foreach ($fetch['response']['data'] as $offer) {
                $rows[] = [
                "offer_id" => $offer['Offer']['id'],
                "offer_name" => utf8_encode($offer['Offer']['name']),
                "price" => $offer['Offer']['default_payout'],
                "network_id" => $network_id,
                "network_platform_id" => $network_platform_id,
                "offer_status" => $offer['Offer']['status']
                ];
            }

                Offer::insert($rows);
        }

        return response()->json([
                'success' => 'true',
                'message' => 'Offers synced successfully!',
            ]);
    }


    public function syncHasOfferStatus()
    {
        $offers = OfferCondition::where('network_platform_id', 'hasoffer')->get()->toArray();

        $NetworkPlatform = NetworkPlatform::with('networks')->where('platform_name','hasoffer')->first();
        $hasoffers = $NetworkPlatform->networks;

        foreach ($offers as $offer) {
             $Row = Offer::where('network_id', $offer['network_id'])->where('offer_id', $offer['offer_id'])->first();

            if (!$Row) {
                OfferCondition::find($offer['id'])->delete();
                continue;
            }

            if ($Row->offer_status != 'inactive') {
                $c = curl_init($offer['url']);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
                curl_exec($c);

                $newUrl = curl_getinfo($c, CURLINFO_EFFECTIVE_URL);

                curl_close($c);

                if (strpos($newUrl, $offer['final_domain'])) {
                    $Row->offer_status = 'active';
                    $Row->save();
                } else {
                    $Row->offer_status = 'inactive';
                    $Row->save();
                }
            }
        }

        return response()->json([
                'success' => 'true',
                'message' => 'Offers synced successfully!',
            ]);
    }

    /* EVERFLOW */

    public function syncEverflow()
    {
        set_time_limit(0);
        ignore_user_abort(1);

        $NetworkPlatform = NetworkPlatform::with('networks')->where('platform_name','everflow')->first();
        $networks = $NetworkPlatform->networks;

        foreach ($networks as $network) {
                $rows = [];

                $api_key = $network['api_key'];
                $network_id = $network['id'];
                $network_platform_id = $network['network_platform_id'];
                $offer_type = $network['name'];

                Offer::where('network_id', $network_id)->delete();

                $url = "https://".$network['domain']."/v1/affiliates/alloffers?page_size=500";

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "x-eflow-api-key: $api_key",
                        "content-type: application/json"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);

                if ($err) {
                    return response()->json(['error'=>$err]);
                } else {
                    $fetch = json_decode($response, true);
                }

            foreach ($fetch['offers'] as $offer) {
                $rows[] = [
                "offer_id" => $offer['network_offer_id'],
                "offer_name" => utf8_encode($offer['name']),
                "offer_url" => $offer['tracking_url'],
                "price" => 0.00,
                "network_id" => $network_id,
                "network_platform_id" => $network_platform_id,
                "offer_status" => $offer['offer_status']
                ];
            }

                Offer::insert($rows);
        }

        return response()->json([
                'success' => 'true',
                'message' => 'Offers synced successfully!',
            ]);
    }



    /* Extras */

    public function offerStatusArray(Request $request)
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $offers = [];
        foreach ($data['offers'] as $offer) {
            $offer_status = (string) Offer::select('offer_status')->where('network_id', $offer['network_id'])->where('offer_id', $offer['offer_id'])->pluck('offer_status');
            $offers[] = ['offer_id'=>$offer['offer_id'],'network_id'=>$offer['network_id'],'status'=>$offer_status];
        }
        return $offers;
    }

    public function offerStatus($type, $offer_id)
    {
        return Offer::select('offer_status')->where('network_id', $type)->where('offer_id', $offer_id)->first();
    }

    public function offerNameList(Request $request)
    {
        $rows = Offer::where('offer_name', 'LIKE', '%'.$request->phrase.'%')->get();
        return $rows;
    }

    public function offerList()
    {   
        $title = "Manage Offers";
        return view('offer.index', compact('title'));
    }

    public function allData()
    {
        return Datatables::of(Offer::query())->make(true);
    }

    public function edit(Request $request)
    {   
        $row = Offer::findOrFail($request->id);
        $form_action =  route('update-offers');
        return view('offer.ajax.edit', compact('row','form_action'));
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $request->validate([
            'flux_page_id' => 'nullable|unique:offers,flux_page_id,'.$id,
        ]);

        $Offer = Offer::findOrFail($request->id);
        $Offer->offer_url = $request->offer_url;
        $Offer->offer_redirect_domain = $request->offer_redirect_domain;
        $Offer->flux_page_id = $request->flux_page_id;
        $Offer->save();

        return "Offer updated successfully!";
    }

    public function FluxPageIdOffer($pageID)
    {
       $FluxOffer = Offer::where('flux_page_id',$pageID)->first()->setAppends([]);

       $data = array(
                "id"=> 0,
                "flux_page_id" => $pageID,
                "offer_id" => 0,
                "offer_type" => "",
                "offer_name" => "",
                "offer_status" => "inactive"
                );

       if(!$FluxOffer or is_null($FluxOffer->offer_status))
            return $data;
        
        return $FluxOffer;   
    }
}
