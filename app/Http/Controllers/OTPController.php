<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Mail;
use App\User;

class OTPController extends Controller
{
    public function generate()
    {
        $uid = Auth::id();
        $otp = rand(123456, 987654);

        $User = User::find($uid);
        $User->otp = $otp;
        $User->save();

        $UserDetails = $User->toArray();

        
        Mail::send('mail.otp', $UserDetails, function ($message) {
             $message->to('alignwebs@gmail.com', 'Lifestylepreneur')->subject('Lifestylepreneur OTP');
             $message->from('info@ascendpublish.com', 'Lifestylepreneur');
        });

        return response('sent', 200);
    }

    public function verify(Request $request)
    {
        $otp = $request->input('otp');

        if (strlen($otp) == 6) {
            $uid = Auth::id();
            $User = User::find($uid);

            if ($User->otp == $otp and !empty($User->otp)) {
                $User->otp = null;
                $User->save();
                $response = "valid";
            } else {
                $response = "Invalid OTP";
            }
        } else {
            $response = "Invalid OTP";
        }

        return response($response, 200);
    }
}
