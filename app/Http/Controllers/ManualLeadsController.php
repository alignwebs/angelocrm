<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Storage;
use Schema;
use App\ManualLead;
use App\Tag;

class ManualLeadsController extends Controller
{
    public function index(Request $request)
    {
        $csv_data = "";
        $headers = "";
        $skippedRows = [];

        if ($request->hasFile('csv_file')) {
            /* Read File and Convert to Array */
            $file = $request->csv_file->path();
            $csv_data = csvToArray($file);

            $totalRows = sizeof($csv_data);
            $insertRows = 0;

            foreach ($csv_data as $row) {
                if ($row['email'] != "") {
                    $data = [
                                        "email"=>$row['email'],
                                        "flux_visitor"=>$row['flux_visitor'],
                                        "offer_name"=>$row['offer_name'],
                                        "payout"=>number_format((float) $row['payout'], 2),
                                        "name"=>$row['Name'],
                                        "s1"=>$row['s1'],
                                        "s2"=>$row['s2'],
                                        "s3"=>$row['s3'],
                                        "s4"=>$row['s4'],
                                        "s5"=>$row['s5'],
                                        "flux_hid"=>$row['flux_hid'],
                                    ];

                    $fcsv_data[] = $data;

                    /* Insert into DB */
                    if (ManualLead::where('email', $row['email'])->where('flux_visitor', $row['flux_visitor'])->count() == 0) {
                        if (ManualLead::insert($data)) {
                            $insertRows++;
                        } else {
                            $skippedRows[] = $data;
                        }
                    } else {
                        $skippedRows[] = $data;
                    }
                }
            }


            $alertMsg = $insertRows." of ".$totalRows." records added successfully! <br>";

            $alertMsg = $alertMsg.sizeof($skippedRows)." records skipped! <br>";

            if (sizeof($skippedRows) > 0) {
                foreach ($skippedRows as $row) {
                    $alertMsg = $alertMsg." Email: ".$row['email']." <br>";
                }
            }



            flash($alertMsg)->success();
        }

        $title = "Manual Leads";
        $total['pending'] = ManualLead::pendingLeads()->count();
        $total['moved'] = ManualLead::where('status', 'moved')->count();
        $total['total'] = ManualLead::all()->count();

        return view('manual', compact('csv_data', 'total','title'));
    }

    public function allData()
    {
        return Datatables::of(ManualLead::query())->make(true);
    }


    public function cleanRecords()
    {
        $rows = ManualLead::pendingLeads();

        if ($rows->count() > 0) {
            foreach ($rows as $row) {
                $email = $row->email;
                $name = $row->name;

                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    echo("$email is not a valid email address<br>");

                    $row->name = $email;
                    $row->email = $name;
                    $row->save();
                }
            }
        }
    }

    public function sync()
    {
        dd("Not Active URL");
        set_time_limit(0);

        $csv_data = ManualLead::pendingLeads();

    
        $totalRows = sizeof($csv_data);
        $syncedRows = 0;

        if (sizeof($csv_data) > 0) {
            $data = [];

            foreach ($csv_data as $row) {
                sleep(1);

                $email = $row['email'];
                $flux_visitor = $row['flux_visitor'];
                $offer_name = $row['offer_name'];
                $payout = $row['payout'];

                if ($email != "") {
                        echo "Syncing Visitor: ".$email."<br>";
                        flush();
                        ob_flush();

                        $curl = curl_init();
                        // Set some options - we are passing in a useragent too here
                        curl_setopt_array($curl, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => 'http://roitracker.io/api/manualSync.php?email='.$email.'&flux_visitor='.$flux_visitor.'&offer_name='.$offer_name.'&payout='.$payout,
                            CURLOPT_USERAGENT => 'Codular Sample cURL Request'
                        ]);
                        // Send the request & save response to $resp
                        $result = curl_exec($curl);
                        


                    if (trim($result) == 'moved' or trim($result) == 'Cannot move a subscriber that is not subscribed.' or trim($result) == 'Subscriber already belongs to this list.') {
                        $status = "moved";
                        $syncedRows++;
                    } else {
                        $status = "pending";
                    }

                        $data[] = ['flux_visitor'=>$flux_visitor, 'status'=>$status];

                        ManualLead::where('flux_visitor', $flux_visitor)->update(['status' => $status, 'log'=>$result]);

                        echo "Result: ".$result."<hr>";

                        flush();
                        ob_flush();

                        // Close request to clear up some resources
                        curl_close($curl);
                } else {
                    echo "Email not found!";
                    flush();
                    ob_flush();
                }
            }

            echo '<div class="alert alert-success">'.$syncedRows." out of ".$totalRows." records synced successfully!".'</div>';
        } else {
            echo "No pending leads found!";
        }
    }



    public function edit(Request $request)
    {
        $id = $request->input('id');

        $row = ManualLead::findOrFail($id);

        $aweber_data = getUserFromAweber($row['email']);


        //dd($aweber_data);
        $row->tags = json_encode($aweber_data['tags']);

        $row->save();

        $row = $row->toArray();

        $aweber_tags = [];

        if (isset($aweber_data['tags'])) {
            foreach ($aweber_data['tags'] as $tag) {
                Tag::firstOrCreate(['name'=>$tag]);
                $aweber_tags[$tag] = $tag;
            }
        }

        $tags = Tag::all()->pluck('name', 'name');
        
        $form_action =  route('update-manual-leads');

        return view('ajax.editlead', compact('row', 'aweber_data', 'tags', 'aweber_tags', 'form_action'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');

        $fields = $request->all();

        $tags_remove = [];

        if ($request->input('aweber_tags')) {
            $aweber_tags = $request->input('aweber_tags');
        } else {
            $aweber_tags = [];
        }

        if (sizeof($aweber_tags) > 0 and sizeof($request->input('tags')) > 0) {
            $tags_remove = array_diff($aweber_tags, $request->input('tags'));
        } else {
            $tags_remove = $aweber_tags;
        }
        
        $fields['tags'] = json_encode($request->input('tags'));

        $query_data = [
            'tags_add'    => $request->input('tags'),
            'tags_remove' => array_values($tags_remove),
            'email'       => $request->input('email'),
        ];

        echo file_get_contents('http://roitracker.io/api/updateUser.php?'.http_build_query($query_data));
        

        Arr::forget($fields, 'aweber_tags');
        $Lead = ManualLead::find($id);
        $Lead->update($fields);

        return "Record updated successfully!";
    }

    public function delete(Request $request)
    {
        $ids = $request->input('ids');

        foreach ($ids as $id) {
            if (ManualLead::find($id)->delete()) {
                echo $id.": Deleted Successfully! <br>";
            } else {
                echo $id.": Cannot Delete! <br>";
            }
       
            flush();
            ob_flush();
            sleep(1);
        }
    }
}
