<?php

namespace App\Http\Controllers;

use App\NetworkPlatform;
use App\Network;
use Illuminate\Http\Request;
use Validator;

class NetworkPlatformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $title = "Manage Network Platform";
        return view('network_platform.index', compact('title'));
    }


    public function list()
    {
        $rows = NetworkPlatform::all();
        
        return view('network_platform.ajax.list', compact('rows'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
                
                'platform_name' => 'required'
              
        ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        

        $NetworkPlatform = new NetworkPlatform;
        
        $NetworkPlatform->platform_name = $request->input('platform_name');
        $NetworkPlatform->save(); 
        

        return response()->json(['success'=>'true','message'=>'Network Platform has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NetworkPlatform  $networkPlatform
     * @return \Illuminate\Http\Response
     */
    public function show(NetworkPlatform $networkPlatform)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NetworkPlatform  $networkPlatform
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,NetworkPlatform $networkPlatform)
    {
        $id = $request->id;

        $platform = NetworkPlatform::where('id',$id)->first();
        
        return view('network_platform.ajax.edit',compact('platform'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NetworkPlatform  $networkPlatform
     * @return \Illuminate\Http\Response
     */
    public function updatePlatform(Request $request, NetworkPlatform $networkPlatform)
    {
        
        $validator = Validator::make($request->all(), [
                
                'platform_name' => 'required'
              
        ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        

        $NetworkPlatform = NetworkPlatform::find($request->id);
        
        $NetworkPlatform->platform_name = $request->input('platform_name');
        $NetworkPlatform->save(); 
        

        return response()->json(['success'=>'true','message'=>'Network Platform has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NetworkPlatform  $networkPlatform
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   

        $NetworkPlatform = NetworkPlatform::where('id',$id);
        $NetworkPlatform->delete();
        Network::where('network_platform_id',$id)->delete();
        flash('Network Platform Deleted Successfully')->success();
        return redirect()->back();
    }
}
