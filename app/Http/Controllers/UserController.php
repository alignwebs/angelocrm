<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Auth;
class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('role:admin');
	}

    public function index()
    {   
    	
        $title = "Manage Users";
        return view('user.index', compact('title'));
    }


    public function list()
    {
        $rows = User::all();
        
        return view('user.ajax.list', compact('rows'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $validator = Validator::make($request->all(), [
                'name' => 'required',               
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'active' => 'required'

         ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        $User = new User;
        
        $User->name = $request->input('name');
        
        $User->email = $request->input('email');
        $User->password = bcrypt($request->input('password'));
        $User->active = $request->input('active');

        $User->save(); 
        $User->assignRole($request->input('role'));
        return response()->json(['success'=>'true','message'=>'User has been added successfully']);
    }

    public function edit(Request $request)
    {   
       
        $id = $request->id;       
        $user = User::where('id',$id)->first();

        return view('user.ajax.edit',compact('user'));
        
    }
    
    public function updateUser(Request $request)
    {   
       $validator = Validator::make($request->all(), [
                'name' => 'required',
                
                'email' => 'required|string|email|unique:users,email,'.$request->id,
                 'active' => 'required'
               

         ]);

        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        $User = User::find($request->id);
        
        $User->name = $request->input('name');
         
        $User->email = $request->input('email');
        $password = $request->input('password');
        
        if(!empty($password))
            $User->password = bcrypt($password);
        
        $User->active = $request->input('active');
        $User->save(); 
        $User->syncRoles($request->input('role'));


        return response()->json(['success'=>'true','message'=>'User has been updated successfully']);
        
    }

    public  function destroy(User $user)
    {
        $id = $user->id;
        $User = User::findOrFail($id);
        $User->delete();
        flash('User Deleted Successfully')->success();
        return redirect()->back();
    }
}
