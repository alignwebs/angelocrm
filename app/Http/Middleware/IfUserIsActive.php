<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IfUserIsActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $is_active = Auth::user()->active;
            if(!$is_active){
                Auth::logout();
                flash('User is inactive.')->error();
                return redirect()->route('login');
            }
        }
        return $next($request);
    }
}
