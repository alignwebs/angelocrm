<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       /* Sync Offer Status */
        $schedule->call('App\Http\Controllers\OfferController@syncCakeOfferStatus')->everyTenMinutes()->runInBackground();

       //$schedule->call('App\Http\Controllers\OfferController@syncCakeOffers')->hourly();

        $schedule->call('App\Http\Controllers\OfferController@syncHasOfferStatus')->everyTenMinutes()->runInBackground();

       /* Sync Offers*/
        $schedule->call('App\Http\Controllers\OfferController@syncHasOffers')->timezone('America/New_York')->twiceDaily(0, 6)->runInBackground();
        $schedule->call('App\Http\Controllers\OfferController@syncHasOffers')->timezone('America/New_York')->twiceDaily(12, 18)->runInBackground();

        $schedule->command('mleds:sync')->everyFiveMinutes()->runInBackground();
        $schedule->command('stats:calculate')->everyFiveMinutes()->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
