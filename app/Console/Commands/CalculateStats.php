<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Lead;
use App\ManualLead;

class CalculateStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate all counts from DB and saves to REDIS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date("Y-m-d");
        
        $pendingLeads = Lead::where('aweber_status','pending')->count();
        $pendingLeadsToday = Lead::where('aweber_status','pending')->where('date',$today)->count();
        
        $pendingManualLeads = ManualLead::where('status','pending')->count();
        $pendingManualLeadsToday = ManualLead::where('status','pending')->whereDate('created_at', $today)->count();

        $buyersFromLeads = Lead::where('aweber_status','moved')->count();
        $buyersFromLeadsToday = Lead::where('aweber_status','moved')->where('date',$today)->count();

        $buyersFromManualLeads = ManualLead::where('status','moved')->count();
        $buyersFromManualLeadsToday = ManualLead::where('status','moved')->whereDate('created_at', $today)->count();

        $totalLeads = Lead::count();
        $totalManualLeads = ManualLead::count();

        Redis::set('stats:pending-leads',$pendingLeads);
        Redis::set('stats:pending-leads-today',$pendingLeadsToday);

        Redis::set('stats:pending-manual-leads',$pendingManualLeads);
        Redis::set('stats:pending-manual-leads-today',$pendingManualLeadsToday);

        Redis::set('stats:buyers-leads',$buyersFromLeads);
        Redis::set('stats:buyers-leads-today',$buyersFromLeadsToday);

        Redis::set('stats:buyers-manual-leads',$buyersFromManualLeads);
        Redis::set('stats:buyers-manual-leads-today',$buyersFromManualLeadsToday);

        Redis::set('stats:total-leads',$totalLeads);
        Redis::set('stats:total-manual-leads',$totalManualLeads);
    }
}
