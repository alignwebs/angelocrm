<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Lead;
use App\ManualLead;
use App\SyncLog;

class SyncMLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mleds:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Manual Pending Leads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);

        $csv_data = ManualLead::pendingLeads()->take(10);
        
        $totalRows = sizeof($csv_data);
        $syncedRows = 0;

        if (sizeof($csv_data) > 0) {
            $data = [];

            foreach ($csv_data as $row) {
                $email = $row['email'];
                $flux_visitor = $row['flux_visitor'];
                $offer_name = $row['offer_name'];
                $payout = $row['payout'];

                if ($email != "") {
                    $curl = curl_init();
                       
                    curl_setopt_array($curl, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => 'http://roitracker.io/api/manualSync.php?email='.urlencode($email).'&flux_visitor='.urlencode($flux_visitor).'&offer_name='.urlencode($offer_name).'&payout='.urlencode($payout),
                    ]);
                     
                    $result = curl_exec($curl);
                    curl_close($curl);


                    if (trim($result) == 'moved' or strpos($result, 'moved') > 0 or trim($result) == 'Cannot move a subscriber that is not subscribed.' or trim($result) == 'Subscriber already belongs to this list.') {
                        $status = "moved";
                        $syncedRows++;
                    } else {
                        $status = "pending";
                    }

                        $data[] = ['flux_visitor'=>$flux_visitor, 'status'=>$status];

                        $mlead = ManualLead::where('flux_visitor', $flux_visitor)->update(['status' => $status, 'log'=>$result]);

                        if($status == 'moved')
                        {
                            # ADD LEAD TO LEADS TABLE
                            $Lead = new Lead;
                            $Lead->email = $mlead->email;
                            $Lead->mobile = $mlead->mobile;
                            $Lead->visitor_id = $mlead->flux_visitor;
                            $Lead->hit_id = $mlead->flux_hid;
                            $Lead->offer_name = $mlead->offer_name;
                            $Lead->payout = $mlead->payout;
                            $Lead->name = $mlead->name;
                            $Lead->tags = $mlead->tags;
                            $Lead->s1 = $mlead->s1;
                            $Lead->s2 = $mlead->s2;
                            $Lead->s3 = $mlead->s3;
                            $Lead->s4 = $mlead->s4;
                            $Lead->s5 = $mlead->s5;
                            $Lead->status = $mlead->status;
                            $Lead->save();

                            # DELETE LEAD FROM MANUAL LEADS TABLE
                            $mlead->delete();
                        }

                        
                } else {
                    $result = "Email not found!";
                }


                $SyncLog = new SyncLog();
                $SyncLog->type = "manual";
                $SyncLog->ref_id = $row['id'];
                $SyncLog->response = $result;
                $SyncLog->status = $status;
                $SyncLog->save();
                
                
                // Loop Ends
            }
        }
    }
}
