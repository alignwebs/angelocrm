<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManualLead extends Model
{
    protected $table = 'mleads';
    
    protected $hidden = [
        'updated_at'
    ];
    protected $guarded = [];
    
    public function getNameAttribute($value)
    {
        return title_case($value);
    }

    public function getPayoutAttribute($value)
    {
        return number_format($value, 2);
    }

    public static function pendingLeads()
    {
        return Self::where('status', 'pending')->get();
    }

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
