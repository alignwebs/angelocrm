<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Network extends Model
{
	use SoftDeletes;
	
    public function network_platform()
  	{
      return $this->belongsTo('App\NetworkPlatform');
  	}

  	public function scopeActive($query)
  	{
      return $query->where('active',1);
  	}
}
