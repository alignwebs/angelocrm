<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leads', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->nullable();
			$table->string('mobile', 20)->nullable();
			$table->string('visitor_id')->nullable();
			$table->string('hit_id')->nullable();
			$table->string('offer_id')->nullable();
			$table->string('offer_name')->nullable();
			$table->string('s1')->nullable();
			$table->string('s2')->nullable();
			$table->string('s3')->nullable();
			$table->string('s4')->nullable();
			$table->string('s5')->nullable();
			$table->string('tags')->nullable();
			$table->string('name')->nullable();
			$table->string('domain')->nullable();
			$table->string('listname')->nullable();
			$table->string('date')->nullable();
			$table->string('time')->nullable();
			$table->string('spot')->nullable();
			$table->string('source')->nullable();
			$table->string('type')->nullable();
			$table->text('aweber_log', 65535)->nullable();
			$table->text('raw', 65535)->nullable();
			$table->string('aweber_status')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->index('visitor_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leads');
	}

}
