<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMleadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mleads', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email', 191)->nullable();
			$table->string('mobile', 100)->nullable();
			$table->string('flux_visitor', 191)->nullable();
			$table->string('flux_hid', 191)->nullable();
			$table->string('offer_name', 191)->nullable();
			$table->double('payout', 10,2)->nullable();
			$table->string('name', 191)->nullable();
			$table->string('tags', 191)->nullable();
			$table->string('s1', 191)->nullable();
			$table->string('s2', 191)->nullable();
			$table->string('s3', 191)->nullable();
			$table->string('s4', 191)->nullable();
			$table->string('s5', 191)->nullable();
			$table->string('status', 191)->default('pending');
			$table->text('log', 65535)->nullable();
			$table->timestamps();

			$table->index('flux_visitor');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mleads');
	}

}
