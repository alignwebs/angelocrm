<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSynclogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('synclogs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type')->nullable();
			$table->integer('ref_id')->nullable();
			$table->text('response', 65535)->nullable();
			$table->string('status')->nullable()->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('synclogs');
	}

}
