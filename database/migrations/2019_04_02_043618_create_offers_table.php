<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOffersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('offer_id')->unsigned()->nullable()->default(0);
			$table->bigInteger('offer_contract_id')->unsigned()->nullable()->default(0);
			$table->integer('campaign_id')->unsigned()->nullable()->default(0);
			$table->string('offer_name')->nullable();
			$table->float('price', 10)->nullable();
			$table->string('network_id')->nullable();
			$table->string('offer_status')->nullable();
			$table->string('network_platform_id')->nullable();
			$table->string('offer_url')->nullable();
			$table->string('offer_redirect_domain')->nullable();
			$table->string('flux_page_id')->unique()->nullable();
			$table->timestamps();
			
			$table->index(['offer_id','network_platform_id']);

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offers');
	}

}
