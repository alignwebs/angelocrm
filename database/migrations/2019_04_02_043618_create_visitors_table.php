<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visitors', function(Blueprint $table)
		{
			$table->bigInteger('visitor_id')->unsigned()->index('visitor_id');
			$table->bigInteger('node_id')->unsigned()->index('node_id');
			$table->boolean('onDoneNumber')->nullable()->default(0);
			$table->string('rotator_type', 50)->nullable();
			$table->string('timestamp', 50);

			$table->index(['visitor_id','node_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visitors');
	}

}
