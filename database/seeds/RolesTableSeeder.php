<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'guard_name' => 'web',
                'created_at' => '2019-04-02 04:50:08',
                'updated_at' => '2019-04-02 04:50:08',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'sub_admin',
                'guard_name' => 'web',
                'created_at' => '2019-04-02 04:50:26',
                'updated_at' => '2019-04-02 04:50:26',
            ),
        ));
        
        
    }
}